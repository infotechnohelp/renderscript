<?php declare(strict_types=1);

namespace VendorTmp\TextModifier;

class PhpStringModifier extends StringModifier
{
    /**
     * @var PhpStringPointer|StringPointer
     */
    protected StringPointer $pointer;

    public function __construct()
    {
        parent::__construct();
        $this->pointer = new PhpStringPointer();
    }

    public function addToClassStringEnd(string $classString, string $insertedString): string
    {
        return $this->insertBeforeLastOccurrence("/}/", $classString, $insertedString);
    }

    public function addToClassStringBeginning(string $classString, string $insertedString): string
    {
        return $this->insertAfterFirstOccurrence("/{/", $classString, $insertedString);
    }

    public function addUseToFileString(string $classString, string $insertedString): ?string
    {
        if ($this->pointer->useSpecified($classString)) {

            return $this->insertAfterLastOccurrence(
                "/[^A-Za-z$.:_-]use.*;/", $classString, "\nuse $insertedString;"
            );

        } elseif ($this->pointer->namespaceSpecified($classString)) {

            return $this->insertAfterLastOccurrence(
                "/[^A-Za-z$.:_-]namespace.*;/", $classString, "\n\nuse $insertedString;"
            );

        } elseif ($this->pointer->isDeclared($classString)) {

            return $this->insertAfterLastOccurrence(
                "/[^A-Za-z$.:_-]declare.*;/", $classString, "\n\nuse $insertedString;"
            );

        }

        return $this->insertAfterLastOccurrence(
            "/[^A-Za-z$.:_-]<?php/", $classString, "\n\nuse $insertedString;"
        );
    }
}