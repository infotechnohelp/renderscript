<?php declare(strict_types=1);

namespace VendorTmp\TextModifier;

class StringModifier
{
    protected StringPointer $pointer;

    public function __construct()
    {
        $this->pointer = new StringPointer();
    }

    public function insertBeforeLastOccurrence(string $needle, string $string, string $insertedString): string
    {
        $pos = $this->pointer->getLastOccurrencePosition($needle, $string);

        return substr_replace($string, $insertedString, $pos, 0);
    }

    public function insertAfterLastOccurrence(string $needle, string $string, string $insertedString): string
    {
        $pos = $this->pointer->getLastOccurrencePosition($needle, $string);
        $occurrence = $this->pointer->getLastOccurrenceString($needle, $string);

        return substr_replace($string, $insertedString, $pos + strlen($occurrence), 0);
    }

    public function insertAfterFirstOccurrence(string $needle, string $string, string $insertedString): string
    {
        $pos = $this->pointer->getFirstOccurrencePosition($needle, $string);
        $occurrence = $this->pointer->getFirstOccurrenceString($needle, $string);

        return substr_replace($string, $insertedString, $pos + strlen($occurrence), 0);
    }

    public static function ltrimDuplicates(string $string, string $characters): string
    {
        if (substr($string, 0, 1) !== $characters) {
            return $string;
        }

        return '_' . ltrim($string, $characters);
    }

    public static function lcfirst(string $string, string $ignoredPrefix = null)
    {
        if (strpos($string, $ignoredPrefix) !== 0) {
            return lcfirst($string);
        }

        $trimmedString = substr($string, strlen($ignoredPrefix));

        return $ignoredPrefix . lcfirst($trimmedString);
    }
}