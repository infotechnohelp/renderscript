<?php /** @noinspection PhpPrivateFieldCanBeLocalVariableInspection */

namespace VendorTmp\TextModifier;

use Infotechnohelp\Filesystem\Filesystem;
use Test\Utility\TestHelper;

class PhpFileModifier extends PhpStringModifier
{
    private bool $return;

    private bool $htmlEntities;

    public function __construct(bool $return = false, bool $htmlEntities = false)
    {
        parent::__construct();

        $this->return = $return;
        $this->htmlEntities = $htmlEntities;
    }


    private function getContents(string $classFilePath): string
    {
        if(!file_exists($classFilePath)){

            throw new \Exception("'$classFilePath' does not exist");
        }

        $classString = file_get_contents($classFilePath);

        if ($this->htmlEntities) {
            return htmlentities($classString);
        }

        return $classString;
    }

    public function addToClassEnd(string $classFilePath, string $insertedString): ?string
    {
        $classString = $this->getContents($classFilePath);

        $result = $this->addToClassStringEnd($classString, $insertedString);

        if ($this->return) {
            return $result;
        }

        TestHelper::preparePath($classFilePath);

        (new Filesystem())->write($classFilePath, $result);

        return null;
    }

    public function addToClassBeginning(string $classFilePath, string $insertedString): ?string
    {
        $classString = $this->getContents($classFilePath);

        $result = $this->addToClassStringBeginning($classString, $insertedString);

        if ($this->return) {
            return $result;
        }

        TestHelper::preparePath($classFilePath);

        (new Filesystem())->write($classFilePath, $result);

        return null;
    }

    public function addUseToFile(string $filePath, string $insertedString): ?string
    {
        $fileString = $this->getContents($filePath);

        $result = $this->addUseToFileString($fileString, $insertedString);

        if ($this->return) {
            return $result;
        }

        TestHelper::preparePath($filePath);

        (new Filesystem())->write($filePath, $result);

        return null;
    }
}