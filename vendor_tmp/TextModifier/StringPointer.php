<?php declare(strict_types=1);

namespace VendorTmp\TextModifier;

class StringPointer
{
    public function getFirstOccurrencePosition(string $regex, string $str): ?int
    {
        if (preg_match_all($regex, $str, $matches, PREG_OFFSET_CAPTURE)) {
            return end($matches[0][0]);
        }

        return null;
    }

    public function getFirstOccurrenceString(string $regex, string $str): ?string
    {
        if (preg_match_all($regex, $str, $matches)) {
            return end($matches[0]);
        }

        return null;
    }

    public function getLastOccurrencePosition(string $regex, string $str): ?int
    {
        if (preg_match_all($regex, $str, $matches, PREG_OFFSET_CAPTURE)) {

            return $matches[0][count($matches[0]) - 1][1];
        }

        return null;
    }

    public function getLastOccurrenceString(string $regex, string $str): ?string
    {
        if (preg_match_all($regex, $str, $matches)) {
            return end($matches[0]);
        }

        return null;
    }

    public function occurenceFound(string $regex, string $string): bool
    {
        preg_match_all("$regex", $string, $matches);

        return !empty($matches[0]);
    }
}