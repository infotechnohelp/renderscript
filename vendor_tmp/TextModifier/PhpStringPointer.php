<?php declare(strict_types=1);

namespace VendorTmp\TextModifier;

class PhpStringPointer extends StringPointer
{
    public function namespaceSpecified(string $string): bool
    {
        return $this->occurenceFound("/[^A-Za-z$.:_-]namespace/", $string);
    }

    public function useSpecified(string $string): bool
    {
        return $this->occurenceFound("/[^A-Za-z$.:_-]use/", $string);
    }

    public function isDeclared(string $string): bool
    {
        return $this->occurenceFound("/[^A-Za-z$.:_-]declare/", $string);
    }
}