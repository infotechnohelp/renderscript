<?php declare(strict_types=1);

namespace VendorTmp\ScriptRunner;

class ScriptRunner
{
    public static function extractParams(array $argv): array
    {
        $result = [];

        foreach ($argv as $item) {
            if (substr($item, 0, 1) === '-') {
                continue;
            }

            $result[] = $item;
        }

        return $result;
    }

    public static function extractOptions(array $argv): array
    {
        $result = [];

        foreach ($argv as $item) {
            if (substr($item, 0, 1) !== '-') {
                continue;
            }

            $result[] = $item;
        }

        return $result;
    }

    public static function parseOptions(array $argv): array
    {
        $result = [];

        foreach ($argv as $item) {
            if (substr($item, 0, 1) !== '-') {
                continue;
            }

            list($title, $value) = explode('=', ltrim($item, '-'));

            $value = in_array($value, ['true', 'false'], true) ?
                filter_var($value, FILTER_VALIDATE_BOOLEAN):
                $value;

            $result[$title][] = $value;
        }

        return $result;
    }
}