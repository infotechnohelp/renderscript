<?php declare(strict_types=1);

namespace Test\Lib;

use Symplify\EasyTesting\PHPUnit\Behavior\DirectoryAssertableTrait;

class IntegrationTestCase extends BaseTestCase
{
    use DirectoryAssertableTrait;

    protected function deleteTmpDir()
    {
        exec("rm -rf " . TEST_DIR . "tmp");
    }

    protected function assertTmpOutputFiles(string $assertionDirTitle)
    {
        echo "$assertionDirTitle\n";

        $this->assertDirectoryEquals(
            TEST_DIR . "TestCases/IntegrationTests/GeneratingFiles/assert/$assertionDirTitle",
            TEST_DIR . "tmp"
        );
    }

    protected function assertOutput(string $__METHOD__, string $expected)
    {
        $this->assertEquals(
            $this->getPredefinedOutput($__METHOD__),
            $expected
        );
    }

    protected function getPredefinedOutput(string $__METHOD__): string
    {
        list($fullClassName, $methodTitle) = explode("::", $__METHOD__);

        $explodedFullClassName = explode("\\", $fullClassName);

        $className = end($explodedFullClassName);

        $notSuffixedClassName =  preg_replace('/Test$/s', '', $className);

        $notPrefixedMethodTitle = lcfirst(preg_replace('/^_test/s', '', $methodTitle));

        $classNamePath = implode('/', preg_split('/(?=[A-Z])/',$notSuffixedClassName));

        return file_get_contents(
            TEST_DIR . "TestCases/IntegrationTests/OutputOnly/assert$classNamePath/$notPrefixedMethodTitle.php"
        );
    }

    protected function _testScope(string $__METHOD__, string $selfClass)
    {
        echo '.' . $__METHOD__ . "\n";

        $class = new \ReflectionClass($selfClass);
        $methods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);

        $tests = [];

        foreach ($methods as $method) {
            if ($method->class !== $selfClass) {
                continue;
            }

            if ($method->name === 'testScope' || preg_match('/^test/s', $method->name)) {
                continue;
            }

            $tests[] = $method->name;
        }

        try {
            foreach ($tests as $test) {
                echo lcfirst(preg_replace('/^_test/s', '', $test)) . " | ";

                $this->$test();
            }
        } catch (\Exception $exception) {
            echo "\n";
            throw $exception;
        }

        echo "\n";
    }

}