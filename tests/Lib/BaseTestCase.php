<?php declare(strict_types=1);

namespace Test\Lib;

use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    protected function getRootPath()
    {
        return dirname(dirname(dirname(__FILE__))) . "/";
    }

    protected function _test(string $__METHOD__, array $input, callable $callable, bool $newLine = false)
    {
        echo ".$__METHOD__";

        echo "\n";

        $callable($input);

        if($newLine){
            echo "\n";
        }
    }
}
