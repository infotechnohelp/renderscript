<?php declare(strict_types=1);

namespace Test\Lib;

class UnitTestCase extends BaseTestCase
{

    protected function getInputFullPath(string $title): string
    {
        return "{$this->getRootPath()}tests/TestCases/UnitTests/assert/input/$title.php";
    }

    protected function getPredefinedOutput(string $__METHOD__, int $i): string
    {
        list($fullClassName, $methodTitle) = explode("::", $__METHOD__);

        $explodedFullClassName = explode("\\", $fullClassName);

        $className = end($explodedFullClassName);

        $notSuffixedClassName =  preg_replace('/Test$/s', '', $className);

        $notPrefixedMethodTitle = lcfirst(preg_replace('/^test/s', '', $methodTitle));

        return file_get_contents(
            "{$this->getRootPath()}tests/TestCases/UnitTests/assert/output/$notSuffixedClassName/$notPrefixedMethodTitle$i.php"
        );
    }

    protected function assertPredefinedOutput(string $actualOutput, string $__METHOD__, int $i)
    {
        $this->assertEquals($this->getPredefinedOutput($__METHOD__, $i), $actualOutput);
    }
}