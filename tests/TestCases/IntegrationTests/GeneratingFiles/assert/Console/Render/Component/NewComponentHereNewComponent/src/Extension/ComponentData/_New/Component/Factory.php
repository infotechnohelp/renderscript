<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\_New\Component;

use RenderScript\Extension\ComponentData\_New\Component\Here\Factory as HereFactory;

class Factory
{

    public function here()
    {
        return new HereFactory();
    }
}