<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\_New\Component\Here;

use RenderScript\Lib\Component;

class NewComponent extends Component
{

    public function __construct(string $componentTitle = null)
    {
        parent::__construct(__CLASS__, $componentTitle);
    }

}