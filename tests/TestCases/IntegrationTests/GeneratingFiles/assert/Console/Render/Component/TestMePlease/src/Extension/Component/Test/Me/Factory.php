<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\Test\Me;

class Factory
{

    public function Please(string $componentTitle = null)
    {
        return new Please($componentTitle);
    }
}