<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\_New;

use RenderScript\Extension\Component\_New\Component\Factory as ComponentFactory;

class Factory
{

    public function component()
    {
        return new ComponentFactory();
    }
}