<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\_New\Component\Here;

use RenderScript\Lib\Component\Data as ComponentData;

class NewComponent extends ComponentData
{
    public function __construct(string $componentTitle = null)
    {
        parent::__construct($componentTitle);
    }

    public function title(string $value): self
    {
        $this->set('title', $value);

        return $this;
    }

}