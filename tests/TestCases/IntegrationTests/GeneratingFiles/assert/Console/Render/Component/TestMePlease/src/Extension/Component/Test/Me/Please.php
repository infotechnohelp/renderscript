<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\Test\Me;

use RenderScript\Lib\Component;

class Please extends Component
{

    public function __construct(string $componentTitle = null)
    {
        parent::__construct(__CLASS__, $componentTitle);
    }

}