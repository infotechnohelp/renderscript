<?php declare(strict_types=1);

namespace RenderScript\Extension\Component;

use RenderScript\Lib\Component as LibComponent;
use RenderScript\Extension\Component\Php\Factory as PhpFactory;
use RenderScript\Extension\Component\Self\Factory as SelfFactory;
use RenderScript\Extension\Component\PlainText\Factory as PlainTextFactory;
use RenderScript\Lib\Component\Base;
use RenderScript\Extension\Component\Test\Factory as TestFactory;

class Factory
{
    public function Base()
    {
        return (new Base())->setType(LibComponent::FILE_TYPE);
    }

    public function php()
    {
        return new PhpFactory();
    }

    public function self()
    {
        return new SelfFactory();
    }

    public function plainText()
    {
        return new PlainTextFactory();
    }

    public function test()
    {
        return new TestFactory();
    }
}