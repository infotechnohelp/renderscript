<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Test\Data;

use RenderScript\Extension\ComponentData\Test\Data\Only\Factory as OnlyFactory;

class Factory
{

    public function only()
    {
        return new OnlyFactory();
    }
}