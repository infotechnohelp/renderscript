<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Test;

use RenderScript\Extension\ComponentData\Test\Me\Factory as MeFactory;

class Factory
{

    public function me()
    {
        return new MeFactory();
    }
}