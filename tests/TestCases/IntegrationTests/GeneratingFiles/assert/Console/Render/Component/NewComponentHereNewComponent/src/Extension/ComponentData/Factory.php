<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData;

use RenderScript\Extension\ComponentData\Php\Factory as PhpFactory;
use RenderScript\Extension\ComponentData\PlainText\Factory as PlainTextFactory;
use RenderScript\Extension\ComponentData\Self\Factory as SelfFactory;
use RenderScript\Extension\ComponentData\_New\Factory as NewFactory;

class Factory
{
    public function php()
    {
        return new PhpFactory();
    }

    public function self()
    {
        return new SelfFactory();
    }

    public function plainText()
    {
        return new PlainTextFactory();
    }

    public function new()
    {
        return new NewFactory();
    }
}