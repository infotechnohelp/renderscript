<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Test\Data\Only;

class Factory
{

    public function Beginning(string $componentTitle = null)
    {
        return new Beginning($componentTitle);
    }
}