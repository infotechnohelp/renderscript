<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\_New\Component\Here;

class Factory
{

    public function NewComponent(string $componentTitle = null)
    {
        return new NewComponent($componentTitle);
    }
}