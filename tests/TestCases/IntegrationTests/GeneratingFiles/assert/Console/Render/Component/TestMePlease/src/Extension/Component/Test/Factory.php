<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\Test;

use RenderScript\Extension\Component\Test\Me\Factory as MeFactory;

class Factory
{

    public function me()
    {
        return new MeFactory();
    }
}