<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Test\Data\Only;

class Factory
{

    public function End(string $componentTitle = null)
    {
        return new End($componentTitle);
    }
}