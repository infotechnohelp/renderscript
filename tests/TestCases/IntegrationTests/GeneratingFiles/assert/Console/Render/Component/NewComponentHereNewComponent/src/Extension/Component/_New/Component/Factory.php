<?php declare(strict_types = 1);

namespace RenderScript\Extension\Component\_New\Component;

use RenderScript\Extension\Component\_New\Component\Here\Factory as HereFactory;

class Factory
{

    public function here()
    {
        return new HereFactory();
    }
}