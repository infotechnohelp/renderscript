<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\_New;

use RenderScript\Extension\ComponentData\_New\Component\Factory as ComponentFactory;

class Factory
{

    public function component()
    {
        return new ComponentFactory();
    }
}