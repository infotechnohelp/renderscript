<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Test;

use RenderScript\Extension\ComponentData\Test\Data\Factory as DataFactory;

class Factory
{

    public function data()
    {
        return new DataFactory();
    }
}