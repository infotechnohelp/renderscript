<?php declare(strict_types = 1);

namespace RenderScript\Extension\Recipe\AnyCategory\AnySubCategory;

use RenderScript\Extension\Recipe\AnyCategory\AnySubCategory\AnyRecipe\Recipe as AnySubCategoryAnyRecipe;

class Factory
{

    public function AnyRecipe(string $fileTitle, string $filePath)
    {
        return new AnySubCategoryAnyRecipe($fileTitle, $filePath);
    }
}