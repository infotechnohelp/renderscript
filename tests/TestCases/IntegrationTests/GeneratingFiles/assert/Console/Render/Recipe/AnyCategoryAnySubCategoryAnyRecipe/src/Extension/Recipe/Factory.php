<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe;

use RenderScript\Extension\Recipe\Self\Factory as SelfFactory;
use RenderScript\Extension\Recipe\Php\Factory as PhpFactory;
use RenderScript\Lib\Recipe\FactoryBase;
use RenderScript\Extension\Recipe\AnyCategory\Factory as AnyCategoryFactory;


class Factory extends FactoryBase
{
    public function self()
    {
        return new SelfFactory();
    }

    public function php()
    {
        return new PhpFactory();
    }

    public function anyCategory()
    {
        return new AnyCategoryFactory();
    }
}