<?php declare(strict_types = 1);

namespace RenderScript\Extension\Recipe\AnyCategory\AnySubCategory\_Namespace;

use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\RecipeInterface;

class Recipe extends BaseRecipe implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return null;
    }
}