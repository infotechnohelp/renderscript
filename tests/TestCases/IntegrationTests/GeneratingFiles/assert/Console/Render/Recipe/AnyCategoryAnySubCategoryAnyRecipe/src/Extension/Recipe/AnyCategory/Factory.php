<?php declare(strict_types = 1);

namespace RenderScript\Extension\Recipe\AnyCategory;

use RenderScript\Extension\Recipe\AnyCategory\AnySubCategory\Factory as AnySubCategoryFactory;

class Factory
{

    public function anySubCategory()
    {
        return new AnySubCategoryFactory();
    }
}