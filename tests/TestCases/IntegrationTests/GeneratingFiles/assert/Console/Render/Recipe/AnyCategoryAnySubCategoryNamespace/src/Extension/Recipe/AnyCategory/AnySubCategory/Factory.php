<?php declare(strict_types = 1);

namespace RenderScript\Extension\Recipe\AnyCategory\AnySubCategory;

use RenderScript\Extension\Recipe\AnyCategory\AnySubCategory\_Namespace\Recipe as AnySubCategoryNamespace;

class Factory
{

    public function Namespace(string $fileTitle, string $filePath)
    {
        return new AnySubCategoryNamespace($fileTitle, $filePath);
    }
}