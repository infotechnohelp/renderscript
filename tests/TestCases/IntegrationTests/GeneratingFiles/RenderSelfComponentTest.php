<?php declare(strict_types=1);

namespace Test\TestCases\IntegrationTests\GeneratingFiles;

use RenderScript\Extension\ComponentData\Factory as ComponentDataFactory;
use RenderScript\Extension\Recipe\Factory as RecipeFactory;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\TwigRenderer\RenderingSettings;
use Test\Lib\IntegrationTestCase;
use VendorTmp\TextModifier\PhpFileModifier;
use RenderScript\Extension\PlainText\Factory as PlainTextFactory;

final class RenderSelfComponentTest extends IntegrationTestCase
{
    private string $assertDirTitle;

    private string $cmd;

    public function testRenderSelfComponent()
    {
        $this->assertDirTitle = 'Console/Render/Component';
        $this->cmd = 'bin/render component';

        $this->_test(__METHOD__, [
            'PhpClassGettersTest' => 'GettersTest Php._Class',
            'TestMePlease' => 'Please Test.Me',
            'NewComponentHereNewComponent' => 'NewComponent _New.Component.Here',
            'TestDataOnlyEnd' => 'End Test.Data.Only --only=data',
            'TestDataOnlyBeginning' => '--only=data Beginning Test.Data.Only',
        ], function ($input) {
            foreach ($input as $testTitle => $cmd) {
                passthru("{$this->getRootPath()}{$this->cmd} $cmd --displayWatermark=false");

                $this->assertTmpOutputFiles("{$this->assertDirTitle}/$testTitle");

                $this->deleteTmpDir();
            }
        });
    }

    public function testRenderScriptNoRewrite()
    {
        $this->assertDirTitle = 'Console/Render/Component';
        $this->cmd = 'bin/render component';

        $this->_test(__METHOD__, [
            'AnyTitle' => ['Any.Path', 'tmp/src/Extension/ComponentData/Any/Path/AnyTitle.php', 'AnyPathAnyTitleNoRewrite'],
        ], function ($input) {
            foreach ($input as $title => $data) {
                list($quickPath, $fullPath, $assertionPath) = $data;

                $df = new ComponentDataFactory();
                $ptf = new PlainTextFactory();

                $RecipeData = (new RecipeData())
                    ->addComponentData($df->php()->file()->Base()
                        ->declareStrict()
                    )
                    ->addComponentData($df->php()->file()->Namespace()
                        ->namespace('RenderScript\\Extension\ComponentData\\' . implode("\\", explode(".", $quickPath)))
                    )
                    ->addComponentData($df->php()->file()->Use()
                        ->addUse('RenderScript\Lib\Component\Data as ComponentData')
                    )
                    ->addComponentData($df->php()->class()->Base()->title($title)
                        ->extends('ComponentData')
                    )
                    ->addComponentData($df->plainText()->Base('Constructor')->text(
                        $ptf->self()->ComponentData()->construct())
                    )
                    ->addComponentData($df->plainText()->Base('Setter method')->text(
                        $ptf->self()->ComponentData()->setterMethod())
                    );

                $rsf = new RecipeFactory();
                $rsf->self()->ComponentData($title, $quickPath)->render($RecipeData);

                (new PhpFileModifier())->addToClassEnd(
                    TEST_DIR . $fullPath, "// Test comment\n"
                );

                $rsf = new RecipeFactory();
                $rsf->self()->ComponentData($title, $quickPath)->render($RecipeData, (new RenderingSettings())->rewrite(false));

                $this->assertTmpOutputFiles("{$this->assertDirTitle}/$assertionPath");

                $this->deleteTmpDir();
            }
        });
    }
}