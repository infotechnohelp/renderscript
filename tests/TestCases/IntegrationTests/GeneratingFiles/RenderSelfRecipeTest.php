<?php declare(strict_types=1);

namespace Test\TestCases\IntegrationTests\GeneratingFiles;

use Test\Lib\IntegrationTestCase;

final class RenderSelfRecipeTest extends IntegrationTestCase
{
    private string $assertDirTitle;

    private string $cmd;

    public function testRenderSelfRecipe()
    {
        $this->assertDirTitle = 'Console/Render/Recipe';
        $this->cmd = 'bin/render recipe';

        $this->_test(__METHOD__, [
            'AnyCategoryAnySubCategoryAnyRecipe' => 'AnyRecipe AnyCategory.AnySubCategory',
            'AnyCategoryAnySubCategoryNamespace' => '_Namespace AnyCategory.AnySubCategory',
        ], function ($input) {
            foreach ($input as $testTitle => $cmd) {
                passthru("{$this->getRootPath()}{$this->cmd} $cmd --displayWatermark=false");

                $this->assertTmpOutputFiles("{$this->assertDirTitle}/$testTitle");

                $this->deleteTmpDir();
            }
        });
    }
}