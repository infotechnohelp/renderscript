<?php declare(strict_types=1);

namespace Test\TestCases\IntegrationTests\OutputOnly;

use RenderScript\Extension\ComponentData\Factory as DataFactory;
use RenderScript\Extension\Recipe\Factory as RecipeFactory;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\TwigRenderer\RenderingSettings;
use Test\Lib\IntegrationTestCase;
use TestApp\Extension\ComponentData\Factory as TestAppDataFactory;
use TestApp\Extension\Recipe\Factory as TestAppRecipeFactory;

final class RenderPhpClassTest extends IntegrationTestCase
{
    public function testScope()
    {
        $this->_testScope(__METHOD__, self::class);
    }

    public function _testMinimumData()
    {
        $rsf = new RecipeFactory();

        $this->assertOutput(
            __METHOD__, $rsf->php()->Class('MyClass', 'Welcome')
            ->render(new RecipeData(), (new RenderingSettings())->return())
        );
    }

    public function _testMaximumData()
    {
        $rsf = new RecipeFactory();
        $df = new DataFactory();

        $PropertiesData =
            $df->php()->class()->Properties()
                ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public'))
                ->addProperty('id')
                ->addProperty('counter',
                    $df->php()->class()->Properties()->Property()
                        ->visibility('private')
                        ->static()
                        ->value(0)
                )
                ->addProperty('title',
                    $df->php()->class()->Properties()->Property()
                        ->value("'Product'")
                );

        $GettersData =
            $df->php()->class()->Getters()
                ->addGetter($df->php()->class()->Getters()->Getter()
                    ->property('title')
                );

        $SettersData =
            $df->php()->class()->Setters()
                ->addSetter(
                    $df->php()->class()->Setters()->Setter()
                        ->property('title')
                        ->returnSelf()
                        ->inputType('string')
                        ->nullableInput()
                );

        $this->assertOutput(
            __METHOD__,
            $rsf->php()->Class('MyClass', 'Welcome')
                ->render(
                    (new RecipeData())
                        ->addComponentData($df->php()->file()->Base()->declareStrict())
                        ->addComponentData($df->php()->file()->Namespace()->namespace('Philipp\\Nikolajev'))
                        ->addComponentData($df->php()->file()->Use()->addUse('RenderScript\\MyParentClass'))
                        ->addComponentData($df->php()->class()->Base()
                            ->extends('MyParentClass')
                            ->abstract()
                            ->implements('AnyInterface')
                        )
                        ->addComponentData($PropertiesData)
                        ->addComponentData($GettersData)
                        ->addComponentData($SettersData),
                    (new RenderingSettings())->return()
                )
        );
    }

    public function _testComponentRecipeItemOrderDoesNotMatter()
    {
        $rsf = new RecipeFactory();
        $df = new DataFactory();

        $PropertiesData =
            $df->php()->class()->Properties()
                ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public'))
                ->addProperty('id')
                ->addProperty('counter',
                    $df->php()->class()->Properties()->Property()
                        ->visibility('private')
                        ->static()
                        ->value(0)
                )
                ->addProperty('title',
                    $df->php()->class()->Properties()->Property()
                        ->value("'Product'")
                );

        $GettersData =
            $df->php()->class()->Getters()
                //->setDefault($df->php()->class()->_Getters()->getDefault())
                ->addGetter($df->php()->class()->Getters()->Getter()
                    ->property('title')
                );

        $SettersData =
            $df->php()->class()->Setters()
                ->addSetter(
                    $df->php()->class()->Setters()->Setter()
                        ->property('title')
                        ->returnSelf()
                        ->inputType('string')
                        ->nullableInput()
                );

        $this->assertOutput(
            __METHOD__,
            $rsf->php()->Class('MyClass', 'Welcome')
                ->render(
                    (new RecipeData())
                        ->addComponentData($SettersData)
                        ->addComponentData($df->php()->file()->Namespace()->namespace('Philipp\\Nikolajev'))
                        ->addComponentData($df->php()->file()->Base()->declareStrict())
                        ->addComponentData($df->php()->file()->Use()->addUse('RenderScript\\MyParentClass'))
                        ->addComponentData($df->php()->class()->Base()
                            ->extends('MyParentClass')
                            ->abstract()
                            ->implements('AnyInterface')
                        )
                        ->addComponentData($GettersData)
                        ->addComponentData($PropertiesData),
                    (new RenderingSettings())->return()
                )
        );
    }

    public function _testDefaultConfigs()
    {
        $rsf = new RecipeFactory();
        $df = new DataFactory();

        $PropertiesData =
            $df->php()->class()->Properties()
                ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public'))
                ->addProperty('id')
                ->addProperty('counter',
                    $df->php()->class()->Properties()->Property()
                        ->visibility('private')
                        ->static()
                        ->value(0)
                )
                ->addProperty('title',
                    $df->php()->class()->Properties()->Property()
                        ->value("'Product'")
                );

        $GettersData =
            $df->php()->class()->Getters()
                ->setDefault($df->php()->class()->Getters()->getDefault()->abstract()->visibility('private')->static())
                ->addGetter($df->php()->class()->Getters()->Getter()
                    ->property('title')
                );

        $SettersData =
            $df->php()->class()->Setters()
                ->setDefault(
                    $df->php()->class()->Setters()->getDefault()
                        ->abstract()
                        ->visibility('private')
                        ->nullableInput(false)
                        ->returnSelf()
                        ->static()
                )
                ->addSetter(
                    $df->php()->class()->Setters()->Setter()
                        ->property('title')
                        ->inputType('string')
                );

        $this->assertOutput(
            __METHOD__,
            $rsf->php()->Class('MyClass', 'Welcome')
                ->render(
                    (new RecipeData())
                        ->addComponentData($df->php()->file()->Base()->declareStrict())
                        ->addComponentData($df->php()->file()->Namespace()->namespace('Philipp\\Nikolajev'))
                        ->addComponentData($df->php()->file()->Use()->addUse('RenderScript\\MyParentClass'))
                        ->addComponentData($df->php()->class()->Base()
                            ->extends('MyParentClass')
                            ->abstract()
                            ->implements('AnyInterface')
                        )
                        ->addComponentData($PropertiesData)
                        ->addComponentData($GettersData)
                        ->addComponentData($SettersData),
                    (new RenderingSettings())->return()
                )
        );
    }

    public function _testTestApp()
    {
        $rsf = new TestAppRecipeFactory();
        $df = new TestAppDataFactory();

        $PropertiesData =
            $df->php()->class()->Properties()
                ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public'))
                ->addProperty('id')
                ->addProperty('counter',
                    $df->php()->class()->Properties()->Property()
                        ->visibility('private')
                        ->static()
                        ->value(0)
                )
                ->addProperty('title',
                    $df->php()->class()->Properties()->Property()
                        ->value("'Product'")
                );

        $GettersData =
            $df->php()->class()->Getters()
                ->addGetter($df->php()->class()->Getters()->Getter()
                    ->property('title')
                );

        $SettersData =
            $df->php()->class()->Setters()
                ->addSetter(
                    $df->php()->class()->Setters()->Setter()
                        ->property('title')
                        ->returnSelf()
                        ->inputType('string')
                        ->nullableInput()
                );

        $this->assertOutput(
            __METHOD__,
            $rsf->php()->Class('MyClass', 'Welcome')
                ->render(
                    (new RecipeData())
                        ->addComponentData($df->php()->file()->Base()->declareStrict())
                        ->addComponentData($df->php()->file()->Namespace()->namespace('Philipp\\Nikolajev'))
                        ->addComponentData($df->php()->file()->Use()->addUse('RenderScript\\MyParentClass'))
                        ->addComponentData($df->php()->class()->Base()
                            ->extends('MyParentClass')
                            ->abstract()
                            ->implements('AnyInterface')
                        )
                        ->addComponentData($PropertiesData)
                        ->addComponentData($GettersData)
                        ->addComponentData($SettersData),
                    (new RenderingSettings())->return()
                )
        );
    }

    public function _testCustomComponentTitles()
    {
        $rsf = new TestAppRecipeFactory();
        $df = new TestAppDataFactory();

        $PropertiesData =
            $df->php()->class()->Properties('Component 4')
                ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public'))
                ->addProperty('id')
                ->addProperty('counter',
                    $df->php()->class()->Properties()->Property()
                        ->visibility('private')
                        ->static()
                        ->value(0)
                )
                ->addProperty('title',
                    $df->php()->class()->Properties()->Property()
                        ->value("'Product'")
                );

        $GettersData =
            $df->php()->class()->Getters('Component 5')
                ->addGetter($df->php()->class()->Getters()->Getter()
                    ->property('title')
                );

        $SettersData =
            $df->php()->class()->Setters('Component 6')
                ->addSetter(
                    $df->php()->class()->Setters()->Setter()
                        ->property('title')
                        ->returnSelf()
                        ->inputType('string')
                        ->nullableInput()
                );

        $this->assertOutput(
            __METHOD__,
            $rsf->php()->Class2('MyClass', 'Welcome')
                ->render(
                    (new RecipeData())
                        ->addComponentData($df->php()->file()->Base('Component 1')
                                ->declareStrict()
                        )
                        ->addComponentData($df->php()->file()->Namespace('Component 2')
                            ->namespace('Philipp\\Nikolajev')
                        )
                        ->addComponentData($df->php()->file()->Use('Component 3')
                            ->addUse('RenderScript\\MyParentClass')
                        )
                        ->addComponentData($df->php()->class()->Base()
                            ->extends('MyParentClass')
                            ->abstract()
                            ->implements('AnyInterface')
                        )
                        ->addComponentData($PropertiesData)
                        ->addComponentData($GettersData)
                        ->addComponentData($SettersData),
                    (new RenderingSettings())->return()
                )
        );
    }

    public function testIncorrectPropertyVisibility()
    {
        echo __METHOD__ . "\n";

        $rsf = new TestAppRecipeFactory();
        $df = new DataFactory();

        $this->expectException(\Exception::class);

        $this->expectExceptionMessage("Incorrect visibility 'private1' provided");

        $PropertiesData =
            $df->php()->class()->Properties()
                ->addProperty('counter',
                    $df->php()->class()->Properties()->Property()
                        ->visibility('private1')
                );

        $rsf->php()->Class3('MyClass', 'Welcome')
            ->render(
                (new RecipeData())->addComponentData($PropertiesData),
                (new RenderingSettings())->return()
            );
    }

    public function testIncorrectPropertyDefaultVisibility()
    {
        echo __METHOD__ . "\n";

        $rsf = new TestAppRecipeFactory();
        $df = new DataFactory();

        $this->expectException(\Exception::class);

        $this->expectExceptionMessage("Incorrect visibility 'public1' provided");

        $PropertiesData =
            $df->php()->class()->Properties()
                ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public1'))
                ->addProperty('counter');

        $rsf->php()->Class3('MyClass', 'Welcome')
            ->render(
                (new RecipeData())->addComponentData($PropertiesData),
                (new RenderingSettings())->return()
            );
    }

    public function testSameCustomComponentTitles()
    {
        echo __METHOD__ . "\n";

        $rsf = new TestAppRecipeFactory();

        $this->expectException(\Exception::class);

        $this->expectExceptionMessage("Duplicated component structure item 'Component 1'");

        $rsf->php()->Class3('MyClass', 'Welcome')
            ->render(
                new RecipeData(),
                (new RenderingSettings())->return()
            );
    }

    public function testDefaultComponentTitles()
    {
        echo __METHOD__ . "\n";

        $rsf = new TestAppRecipeFactory();

        $this->expectException(\Exception::class);

        $this->expectExceptionMessage("Duplicated component structure item 'Php.File.Base'");

        $rsf->php()->Class4('MyClass', 'Welcome')
            ->render(
                new RecipeData(),
                (new RenderingSettings())->return()
            );
    }

    public function testSameCustomRecipeDataComponentTitles()
    {
        echo __METHOD__ . "\n";

        $rsf = new TestAppRecipeFactory();
        $df = new TestAppDataFactory();

        $this->expectException(\Exception::class);

        $this->expectExceptionMessage("Duplicated recipe data item 'Component 1'");

        $rsf->php()->Class2('MyClass', 'Welcome')
            ->render(
                (new RecipeData())
                    ->addComponentData($df->php()->file()->Base('Component 1')
                        ->declareStrict()
                    )
                    ->addComponentData($df->php()->file()->Namespace('Component 1')
                        ->namespace('Philipp\\Nikolajev')
                    ),
                (new RenderingSettings())->return()
            );
    }

    public function testSameDefaultRecipeDataComponentTitles()
    {
        echo __METHOD__ . "\n";

        $rsf = new TestAppRecipeFactory();
        $df = new TestAppDataFactory();

        $this->expectException(\Exception::class);

        $this->expectExceptionMessage("Duplicated recipe data item 'Php.File.Base'");

        $rsf->php()->Class2('MyClass', 'Welcome')
            ->render(
                (new RecipeData())
                    ->addComponentData($df->php()->file()->Base()
                        ->declareStrict()
                    )
                    ->addComponentData($df->php()->file()->Base()
                        ->declareStrict()
                    ),
                (new RenderingSettings())->return()
            );
    }
}