<?php declare(strict_types = 1);

namespace Philipp\Nikolajev;

use RenderScript\MyParentClass;

abstract class MyClass extends MyParentClass implements AnyInterface
{
    public $id;

    private static $counter = 0;

    public $title = 'Product';


    abstract private static function getTitle()
    {
        return $this->title;
    }

    abstract private static function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}