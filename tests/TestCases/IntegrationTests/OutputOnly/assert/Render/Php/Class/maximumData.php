<?php declare(strict_types = 1);

namespace Philipp\Nikolajev;

use RenderScript\MyParentClass;

abstract class MyClass extends MyParentClass implements AnyInterface
{
    public $id;

    private static $counter = 0;

    public $title = 'Product';


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title = null): self
    {
        $this->title = $title;

        return $this;
    }
}