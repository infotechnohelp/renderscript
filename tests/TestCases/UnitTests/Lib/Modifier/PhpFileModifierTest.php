<?php declare(strict_types=1);

namespace Test\TestCases\UnitTests\Lib\Modifier;

use VendorTmp\TextModifier\PhpFileModifier;
use Test\Lib\UnitTestCase;

final class PhpFileModifierTest extends UnitTestCase
{
    public function testAddToClassEnd()
    {
        $__METHOD__ = __METHOD__;

        $this->_test(__METHOD__, ['EmptyClass', 'EmptyClassOneMethod'], function ($input) use ($__METHOD__){
            $i = 1;

            foreach ($input as $item) {

                echo "$item | ";

                $result = (new PhpFileModifier(true))->addToClassEnd(
                    $this->getInputFullPath($item),
                    "//Test string\n"
                );

                $this->assertPredefinedOutput($result, $__METHOD__, $i);

                $i++;
            }
        }, true);
    }

    public function testAddToClassBeginning()
    {
        $__METHOD__ = __METHOD__;

        $this->_test(__METHOD__, ['EmptyClass', 'EmptyClassOneMethod'], function ($input) use ($__METHOD__){
            $i = 1;

            foreach ($input as $item) {

                echo "$item | ";

                $result = (new PhpFileModifier(true))->addToClassBeginning(
                    $this->getInputFullPath($item),
                    "//Test string\n"
                );

                $this->assertPredefinedOutput($result, $__METHOD__, $i);

                $i++;
            }
        }, true);
    }

    public function testAddUseToFile()
    {
        $__METHOD__ = __METHOD__;

        $this->_test(__METHOD__, [
            'EmptyClass',
            'TestClassDeclared',
            'TestClassDeclared2',
            'TestClassNamespace',
            'TestClassUse'
        ], function ($input) use ($__METHOD__){
            $i = 1;

            foreach ($input as $item) {

                echo "$item | ";

                $result = (new PhpFileModifier(true))->addUseToFile(
                    $this->getInputFullPath($item),
                    "TestString"
                );

                $this->assertPredefinedOutput($result, $__METHOD__, $i);

                $i++;
            }
        }, true);
    }

}