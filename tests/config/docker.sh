#!/bin/bash
# We need to install dependencies only for Docker
[[ ! -e /.dockerinit ]] && exit 0

set -xe
apt-get update -yqq
apt-get install git -yqq
apt-get install libicu-dev -yqq
apt-get install zlib1g-dev -yqq
apt-get install libxml2-dev -yqq
apt-get install libzip-dev -yqq

# PHP Extensions
docker-php-ext-install intl
docker-php-ext-install zip

# Composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
