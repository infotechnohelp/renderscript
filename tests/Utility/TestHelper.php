<?php declare(strict_types=1);

namespace Test\Utility;

class TestHelper
{
    public static function preparePath(&$path, bool $return = false)
    {
        $result = $path;

        if (getenv('RENDERSCRIPT_TEST') && strpos($path, getenv('RENDERSCRIPT_TEST')) === false) {
            $result = str_replace(getenv('RENDERSCRIPT_ROOT'), getenv('RENDERSCRIPT_TEST'), $path);
        }

        if ($return) {
            return $result;
        }

        $path = $result;
    }
}