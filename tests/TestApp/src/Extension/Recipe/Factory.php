<?php declare(strict_types=1);

namespace TestApp\Extension\Recipe;

use TestApp\Extension\Recipe\Php\Factory as PhpFactory;
use RenderScript\Lib\Recipe\FactoryBase;


class Factory extends FactoryBase
{
    public function php()
    {
        return new PhpFactory();
    }
}