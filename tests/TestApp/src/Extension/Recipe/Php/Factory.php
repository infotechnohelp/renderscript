<?php declare(strict_types = 1);

namespace TestApp\Extension\Recipe\Php;

use TestApp\Extension\Recipe\Php\_Class\Recipe as PhpClass;
use TestApp\Extension\Recipe\Php\Class2\Recipe as PhpClass2;
use TestApp\Extension\Recipe\Php\Class3\Recipe as PhpClass3;
use TestApp\Extension\Recipe\Php\Class4\Recipe as PhpClass4;

class Factory
{
    public function Class(string $fileTitle, string $filePath)
    {
        return new PhpClass($fileTitle, $filePath);
    }

    public function Class2(string $fileTitle, string $filePath)
    {
        return new PhpClass2($fileTitle, $filePath);
    }

    public function Class3(string $fileTitle, string $filePath)
    {
        return new PhpClass3($fileTitle, $filePath);
    }
    public function Class4(string $fileTitle, string $filePath)
    {
        return new PhpClass4($fileTitle, $filePath);
    }
}