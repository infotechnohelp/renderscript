<?php declare(strict_types=1);

namespace TestApp\Extension\Recipe\Php\_Class;

use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\ComponentStructure\Item as ComponentStructureItem;
use RenderScript\Lib\Component\Data as ComponentData;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\Recipe\RecipeInterface;

class Recipe extends BaseRecipe implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return (new ComponentStructure())
            ->addChild(
                (new ComponentStructureItem($this->cf->php()->file()->Base()))
                    ->addChild(new ComponentStructureItem($this->cf->php()->file()->Namespace()))
                    ->addChild(new ComponentStructureItem($this->cf->php()->file()->Use()))
                    ->addChild(
                        (new ComponentStructureItem($this->cf->php()->class()->Base()))
                            ->addDataModifier(function (ComponentData $Data){
                                $Data->title($this->fileTitle);
                            })
                            ->addChild(new ComponentStructureItem($this->cf->php()->class()->Properties()))
                            ->addChild(new ComponentStructureItem($this->cf->php()->class()->Getters()))
                            ->addChild(new ComponentStructureItem($this->cf->php()->class()->Setters()))
                    )
            );
    }
}