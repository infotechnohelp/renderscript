<?php declare(strict_types = 1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Getter extends ComponentData
{
    public function static(bool $value = true): self
    {
        $this->set('static', $value);

        return $this;
    }

    public function visibility(string $value): self
    {
        $this->set('visibility', $value);

        return $this;
    }

    public function abstract(bool $value = true): self
    {
        $this->set('abstract', $value);

        return $this;
    }

    public function property(string $value = null): self
    {
        $this->set('property', $value);

        return $this;
    }

}