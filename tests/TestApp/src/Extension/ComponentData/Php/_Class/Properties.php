<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Properties extends ComponentData implements \JsonSerializable
{
    private PropertiesDefault $default;

    private array $properties = [];

    public function __construct(string $componentTitle = null)
    {
        $this->default = new PropertiesDefault();

        parent::__construct($componentTitle);
    }

    public function setDefault(PropertiesDefault $default): self
    {
        $this->default = $default;

        return $this;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function Property()
    {
        return new Property();
    }

    public function addProperty(string $title, Property $property = null): self
    {
        $property = ($property === null) ? new Property() : $property;

        $this->properties[$title] = $property;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'default' => $this->default,
            'properties' => $this->properties,
        ];
    }

}