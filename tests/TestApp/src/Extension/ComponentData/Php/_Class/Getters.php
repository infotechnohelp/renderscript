<?php declare(strict_types = 1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Getters extends ComponentData
{
    private GettersDefault $default;

    private array $getters = [];

    public function __construct(string $componentTitle = null)
    {
        $this->default = new GettersDefault();

        parent::__construct($componentTitle);
    }

    public function setDefault(GettersDefault $default): self
    {
        $this->default = $default;

        return $this;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function Getter()
    {
        return new Getter();
    }

    public function addGetter(Getter $getter = null): self
    {
        $getter = ($getter === null) ? new Getter() : $getter;

        $this->getters[] = $getter;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'default' => $this->default,
            'getters' => $this->getters,
        ];
    }

}