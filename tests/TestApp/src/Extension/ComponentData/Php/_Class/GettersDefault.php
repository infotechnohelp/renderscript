<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class GettersDefault extends ComponentData
{
    public function __construct(string $componentTitle = null)
    {
        $this
            ->static(false)
            ->visibility('public')
            ->abstract(false);

        parent::__construct($componentTitle);
    }

    public function static(bool $value = true): self
    {
        $this->set('static', $value);

        return $this;
    }

    public function visibility(string $value): self
    {
        $this->set('visibility', $value);

        return $this;
    }

    public function abstract(bool $value = true): self
    {
        $this->set('abstract', $value);

        return $this;
    }

}