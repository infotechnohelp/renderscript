<?php declare(strict_types = 1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Setter extends ComponentData
{
    public function static(bool $value = true): self
    {
        $this->set('static', $value);

        return $this;
    }

    public function visibility(string $value): self
    {
        $this->set('visibility', $value);

        return $this;
    }

    public function abstract(bool $value = true): self
    {
        $this->set('abstract', $value);

        return $this;
    }

    public function returnSelf(bool $value = true): self
    {
        $this->set('returnSelf', $value);

        return $this;
    }

    public function property(string $value = null): self
    {
        $this->set('property', $value);

        return $this;
    }

    public function inputType(string $value): self
    {
        $this->set('inputType', $value);

        return $this;
    }

    public function nullableInput(bool $value = true): self
    {
        $this->set('nullableInput', $value);

        return $this;
    }
}