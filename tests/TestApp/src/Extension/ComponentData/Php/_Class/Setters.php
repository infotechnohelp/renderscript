<?php declare(strict_types = 1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Setters extends ComponentData
{
    private SettersDefault $default;

    private array $setters = [];

    public function __construct(string $componentTitle = null)
    {
        $this->default = new SettersDefault();

        parent::__construct($componentTitle);
    }

    public function setDefault(SettersDefault $default): self
    {
        $this->default = $default;

        return $this;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function Setter()
    {
        return new Setter();
    }

    public function addSetter(Setter $setter = null): self
    {
        $setter = ($setter === null) ? new Setter() : $setter;

        $this->setters[] = $setter;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'default' => $this->default,
            'setters' => $this->setters,
        ];
    }

}