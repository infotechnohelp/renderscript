<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class PropertiesDefault extends ComponentData
{
    public function __construct(string $componentTitle = null)
    {
        $this
            ->static(false)
            ->visibility('private');

        parent::__construct($componentTitle);
    }

    public function static(bool $value = true): self
    {
        $this->set('static', $value);

        return $this;
    }

    public function visibility(string $value): self
    {
        if(!in_array($value, ['private', 'public', 'protected'])){
            throw new \Exception("Incorrect visibility '$value' provided");
        }

        $this->set('visibility', $value);

        return $this;
    }
}