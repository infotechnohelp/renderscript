<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Property extends ComponentData
{

    public function static(bool $value = true): self
    {
        $this->set('static', $value);

        return $this;
    }

    public function visibility(string $value): self
    {
        if(!in_array($value, ['private', 'public', 'protected'])){
            throw new \Exception("Incorrect visibility '$value' provided");
        }

        $this->set('visibility', $value);

        return $this;
    }

    public function value($value): self
    {
        $this->set('value', $value);

        return $this;
    }
}

