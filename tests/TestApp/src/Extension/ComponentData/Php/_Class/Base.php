<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Base extends ComponentData
{
    public function __construct(string $componentTitle = null)
    {
        $this->abstract(false);

        parent::__construct($componentTitle);
    }

    public function abstract(bool $value = true): self
    {
        $this->set('abstract', $value);

        return $this;
    }

    public function title(string $value): self
    {
        $this->set('title', $value);

        return $this;
    }

    public function implements(string $value): self
    {
        $this->set('implements', $value);

        return $this;
    }

    public function extends(string $value): self
    {
        $this->set('extends', $value);

        return $this;
    }
}