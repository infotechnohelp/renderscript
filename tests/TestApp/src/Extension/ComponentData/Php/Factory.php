<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData\Php;

use TestApp\Extension\ComponentData\Php\File\Factory as PhpFileFactory;
use TestApp\Extension\ComponentData\Php\_Class\Factory as PhpClassFactory;

class Factory
{
    public function file()
    {
        return new PhpFileFactory();
    }

    public function class()
    {
        return new PhpClassFactory();
    }
}