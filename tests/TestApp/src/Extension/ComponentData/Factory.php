<?php declare(strict_types=1);

namespace TestApp\Extension\ComponentData;

use TestApp\Extension\ComponentData\Php\Factory as PhpFactory;

class Factory
{
    public function php()
    {
        return new PhpFactory();
    }
}