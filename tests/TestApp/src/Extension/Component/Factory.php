<?php declare(strict_types=1);

namespace TestApp\Extension\Component;

use TestApp\Extension\Component\Php\Factory as PhpFactory;

class Factory
{
    public function php()
    {
        return new PhpFactory();
    }
}