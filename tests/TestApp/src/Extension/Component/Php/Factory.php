<?php declare(strict_types=1);

namespace TestApp\Extension\Component\Php;

use TestApp\Extension\Component\Php\_Class\Factory as PhpClassFactory;
use TestApp\Extension\Component\Php\File\Factory as PhpFileFactory;

class Factory
{
    public function file()
    {
        return new PhpFileFactory();
    }

    public function class()
    {
        return new PhpClassFactory();
    }
}