<?php declare(strict_types = 1);

namespace TestApp\Extension\Component\Php\_Class;

use RenderScript\Lib\Component;

class Setters extends Component
{
    public function __construct(string $componentTitle = null)
    {
        parent::__construct(__CLASS__, $componentTitle);
    }
}