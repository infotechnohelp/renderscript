<?php declare(strict_types=1);

require_once dirname(__DIR__) . "/vendor/autoload.php";
require_once dirname(__DIR__) . "/bootstrap.php";

use Dotenv\Dotenv;

$Dotenv = new Dotenv(dirname(__FILE__) . "/config");

$Dotenv->load();

$Dotenv->required(['RENDERSCRIPT_TEST', 'RENDERSCRIPT_ROOT']);

define('TEST_DIR', __DIR__ . DIRECTORY_SEPARATOR);