## Install 

[composer](https://composer.org) required

### RenderScript client

`composer require renderscript/renderscript`

### RenderScript engine
##### [ PHP 7.4, Twig 3, CakePhp Utilities 4.1 ?]

`composer require renderscript/engine`

[Read more](https://renderscript.com)

In order to avoid compatibility issues do not install it directly into project's vendor 
but use a separate pre-defined container (from any parent folder of your project)

#### Docker container 
##### [ LINUX, iOS ]

[docker-compose](https://docker-compose.org) required

`composer require renderscript/docker-compose` (run from any parent folder of your project)

[Read more](https://renderscript.com/docker-compose)


#### XAMPP 7.4 +
##### [ WINDOWS ]

[xampp.org](https://xampp.org)

`composer require renderscript/xampp` (run from XAMPP root or htdocs ? folder)

[Read more](https://renderscript.com/xampp)


## About

RenderScript is a framework for automatic file rendering

It consists of n components: 

#### Example

Let's assume we have created our custom Model config class 

(YAML example might be better or use both)

```php
$c = new TableConfigFactory();
$rel = new TableRelationFactory();

$Users = Model::pluginTable('Users', 'Infotechnohelp/Authentication');

$UserGroups = Model::newTable('UserGroups')
    ->addRelation($rel->manyToMany($Users, $c->Relation()->nullable()))
    ->addField('title', fType::string());
```

This framework powers up creation of instructions (RenderScript Recipe -> RenderScript plugin) and templates (Twig Template) 
in order to render a bunch of files sharing same structure logics, any custom config can be used.

Twig is a php template renderer developed by Symfony Framework team

...

Cakephp Utility lib used. We have a warm feelings against CakePhp paradigms (Conventions over configuration)

RenderScript was inspired by CakePhp and Twig.

RenderScript team aims at creation of plugins for CakePHP (as a first official release ?)

The most common use might be CRUD rendering, etc. ? Phinx, CRUD, MVC Framework model (only?) layers

Let's take a small goal as an example. You need to create phinx migration for your project from scratch.
You have to do it for each project (let's assume you have lots of them), and it is a pain, 
lots of obsolete routine required. 

We understand that we are creating files sharing the same structure logic and input data is many times 
smaller than we have to write by hands each time.

Phinx is a lib for writing database migrations (MySQL, etc. ?)

Using this custom config 

#### Aim

Get rid of routine in code writing process by adding a new layer of logics (instructions) created ones 
and used undefined times (auto creation for files sharing same structure patterns) 







