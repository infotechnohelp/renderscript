<?php declare(strict_types=1);

require_once dirname(dirname(dirname(__DIR__))) . '/vendor/autoload.php';
require_once dirname(dirname(dirname(__DIR__))) . '/bootstrap.php';

use RenderScript\Extension\ComponentData\Factory as ComponentDataFactory;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Extension\PlainText\Factory as PlainTextFactory;
use RenderScript\Extension\Recipe\Factory as RecipeFactory;
use VendorTmp\ScriptRunner\ScriptRunner;

list($script, $fileTitle, $filePath) = ScriptRunner::extractParams($argv);

$rf = new RecipeFactory();
$df = new ComponentDataFactory();
$ptf = new PlainTextFactory();

$RecipeData = (new RecipeData())
    ->addComponentData(
        $df->php()->file()->Base()
            ->declareStrict()
    )
    ->addComponentData(
        $df->php()->file()->Namespace()
            ->namespace(
                'RenderScript\\Extension\\Recipe\\' . implode("\\", explode(".", $filePath)) . "\\$fileTitle"
            )
    )
    ->addComponentData(
        $df->php()->file()->Use()
            ->addUse('RenderScript\Lib\Recipe as BaseRecipe')
            ->addUse('RenderScript\Lib\Recipe\ComponentStructure')
            ->addUse('RenderScript\Lib\Recipe\RecipeInterface')
    )
    ->addComponentData(
        $df->php()->class()->Base()
            ->title('Recipe')
            ->extends('BaseRecipe')
            ->implements('RecipeInterface')
    )
    ->addComponentData(
        $df->plainText()->Base('Php Class')
            ->text($ptf->self()->Recipe()->recipeBase())
    );

$rf->self()->Recipe($fileTitle, $filePath)->render($RecipeData);

$rf->self()->factory()->Recipe($fileTitle, $filePath)->render();