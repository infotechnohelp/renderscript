<?php declare(strict_types=1);

require_once dirname(dirname(dirname(__DIR__))) . '/vendor/autoload.php';
require_once dirname(dirname(dirname(__DIR__))) . '/bootstrap.php';

use RenderScript\Extension\ComponentData\Factory as ComponentDataFactory;
use RenderScript\Extension\PlainText\Factory as PlainTextFactory;
use RenderScript\Extension\Recipe\Factory as RecipeFactory;
use RenderScript\Lib\Recipe\Data as RecipeData;
use VendorTmp\ScriptRunner\ScriptRunner;

list($script, $title, $path) = ScriptRunner::extractParams($argv);

$options = ScriptRunner::parseOptions($argv);

$rf = new RecipeFactory();
$ptf = new PlainTextFactory();
$df = new ComponentDataFactory();

$ignoreFactory = isset($options['ignore']) && in_array('factory', $options['ignore'], true);


$RecipeData = (new RecipeData())
    ->addComponentData($df->php()->file()->Base()
        ->declareStrict()
    )
    ->addComponentData($df->php()->file()->Namespace()
        ->namespace('RenderScript\\Extension\ComponentData\\' . implode("\\", explode(".", $path)))
    )
    ->addComponentData($df->php()->file()->Use()
        ->addUse('RenderScript\Lib\Component\Data as ComponentData')
    )
    ->addComponentData($df->php()->class()->Base()->title($title)
        ->extends('ComponentData')
    )
    ->addComponentData($df->plainText()->Base('Constructor')->text(
        $ptf->self()->ComponentData()->construct())
    )
    ->addComponentData($df->plainText()->Base('Setter method')->text(
        $ptf->self()->ComponentData()->setterMethod())
    );

$rf->self()->ComponentData($title, $path)->render($RecipeData);


if (!$ignoreFactory) {
    $rf->self()->factory()->ComponentData($title, $path)->render();
}

if (isset($options['only']) && in_array('data', $options['only'], true)) {
    exit;
}

$filePath = SRC_DIR . "Extension/Component/" . implode("/", explode(".", $path)) . "/";

$selfComponentRecipeData = (new RecipeData())
    ->addComponentData(
        $df->php()->file()->Base()
            ->declareStrict()
    )
    ->addComponentData(
        $df->php()->file()->Namespace()
            ->namespace(
                'RenderScript\\Extension\Component\\' . implode("\\", explode(".", $path))
            )
    )
    ->addComponentData(
        $df->php()->file()->Use()
            ->addUse('RenderScript\Lib\Component')
    )
    ->addComponentData(
        $df->php()->class()->Base()
            ->title($title)
            ->extends('Component')
    )
    ->addComponentData(
        $df->self()->component()->Base()
            ->title($title)
    );

$selfTemplateRecipeData = (new RecipeData())
    ->addComponentData(
        $df->plainText()->Base()->text('Twig template')
    );

$rf->self()->Component($title, $path)->render($selfComponentRecipeData);

$rf->self()->Template($title, $path)->render($selfTemplateRecipeData);

if (!$ignoreFactory) {
    $rf->self()->factory()->Component($title, $path)->render();
}
