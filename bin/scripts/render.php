<?php declare(strict_types=1);

$scriptsDirPath = __DIR__;
$binDirPath = dirname($scriptsDirPath);

require_once dirname($binDirPath) . '/vendor/autoload.php';

use VendorTmp\ScriptRunner\ScriptRunner;

$extractedOptions = ScriptRunner::extractOptions($argv);
$parsedOptions = ScriptRunner::parseOptions($argv);

$displayWatermark = true;

if(isset($parsedOptions['displayWatermark']) && in_array(false, $parsedOptions['displayWatermark'], true)){
    $displayWatermark = false;
}

if ($displayWatermark) {
    echo file_get_contents("$binDirPath/data/watermark.txt");
}

if (count($argv) < 4) {
    echo 'Incomplete input: $script, $arguments...' . "\n";
    exit;
}

list($scriptPath, $script, $argument1, $argument2) = ScriptRunner::extractParams($argv);


switch ($script) {
    case 'component':
        passthru("php $scriptsDirPath/self/add_component.php $argument1 $argument2 " . implode(' ', $extractedOptions));
        break;
    case 'recipe':
        passthru("php $scriptsDirPath/self/add_recipe.php $argument1 $argument2 " . implode(' ', $extractedOptions));
        break;
    default:
        echo "No script with type '$script'\n";
}

