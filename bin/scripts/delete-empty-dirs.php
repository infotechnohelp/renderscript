<?php declare(strict_types=1);

$scriptsDirPath = __DIR__;
$binDirPath = dirname($scriptsDirPath);
$rootPath = dirname($binDirPath);

require_once dirname($binDirPath) . '/vendor/autoload.php';

use Infotechnohelp\Filesystem\Filesystem;

function isIgnored(string $path, array $ignored): bool
{
    foreach ($ignored as $ignoredPath) {
        if (strpos($path, $ignoredPath) === 0) {
            return true;
        }
    }

    return false;
}

$ignored = ["$rootPath/vendor", "$rootPath/tmp"];

$Filesystem = new Filesystem();

$Filesystem->iterateDirectorySubdirectories($rootPath, function ($path) use ($Filesystem, $ignored) {

    if (isIgnored($path, $ignored)) {
        return;
    }

    if ($Filesystem->countDirectoryItems($path) === 0) {
        $Filesystem->rmdirRecursive($path);
    }

}, true, true);