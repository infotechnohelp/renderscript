<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/bootstrap.php';

$loader = new \Twig\Loader\FilesystemLoader(ROOT_DIR);
$twig = new \Twig\Environment($loader, [
    'cache' => ROOT_DIR . "tmp/compilation_cache",
    'debug' => true,
]);

$pathPrefix = 'src/Extension/templates/';
?>

<?php

$input = [
    'config' => [
        'title' => 'ProductsTable.php',
        'include' => ['phpFile'],
    ],
    'components' => [
        'phpFile' => [
            'config' => [
                'template' => $pathPrefix . 'php/file/base.twig',
                'include' => ['phpFileWatermark', 'phpFileNamespace', 'phpFileUse', 'phpClass'],
            ],
            'data' => [
                'declareStrict' => true,
            ],
        ],
        'phpFileWatermark' => [
            'config' => [
                'template' => $pathPrefix . 'php/file/watermark.twig',
            ],
            'data' => [
                'watermark' => 'RenderScript.com 2020',
            ],
        ],
        'phpFileNamespace' => [
            'config' => [
                'template' => $pathPrefix . 'php/file/_namespace.twig',
            ],
            'data' => [
                'namespace' => 'Infotechnohelp\Cakephp',
            ],
        ],
        'phpFileUse' => [
            'config' => [
                'template' => $pathPrefix . 'php/file/_use.twig',
            ],
            'data' => [
                'use' => ['Infotechnohelp\Filesystem', 'Cakephp\Table'],
            ],
        ],
        'phpClass' => [
            'config' => [
                'template' => $pathPrefix . 'php/_class/base.twig',
                'include' => ['phpClassProperties'],
            ],
            'data' => [
                'title' => 'ProductsTable',
                'extends' => 'Table',
            ],
        ],
        'phpClassProperties' => [
            'config' => [
                'template' => $pathPrefix . 'php/_class/properties.twig',
            ],
            'data' => [
                'default' => [
                    'visibility' => 'private',
                    'static' => false,
                ],
                'properties' => [
                    'id' => [],
                    'counter' => [
                        'visibility' => 'public',
                        'static' => true,
                        'value' => 0,
                    ],
                    'title' => [
                        'value' => "'Product'",
                    ],
                ]
            ],
        ],
    ]
];
?>

<pre><?= htmlentities($twig->render('src/Lib/templates/base.twig', $input)) ?></pre>