<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/bootstrap.php';

use Infotechnohelp\Filesystem\Filesystem;

(new Filesystem())->iterateDirectoryFiles(__DIR__, function ($file) {
    if (in_array(basename($file), ['index.php', 'bootstrap.php'], true)) {
        return;
    }

    echo "<div><a href='" . basename($file) . "'>" . explode(".php", basename($file))[0] . "</a></div>";
});