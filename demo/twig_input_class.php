<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/bootstrap.php';

use RenderScript\Extension\Component\Factory as ComponentFactory;
use RenderScript\Extension\ComponentData\Factory as DataFactory;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

$cf = new ComponentFactory();
$df = new DataFactory();


$File = $cf->Base()
    ->setFileInfo('TestClass.php', '/path/to/file/')
    ->reserveIncluded($cf->php()->file()->Base());

$PhpFile = $cf->php()->file()->Base()
    ->setData($df->php()->file()->Base()->declareStrict())
    ->reserveIncluded($cf->php()->file()->Watermark())
    ->reserveIncluded($cf->php()->file()->Namespace())
    ->reserveIncluded($cf->php()->file()->Use())
    ->reserveIncluded($cf->php()->class()->Base());

$File->addComponent($PhpFile);

$Watermark = $cf->php()->file()->Watermark()
    ->setData($df->php()->file()->Watermark()->watermark('RenderScript.com'));

$File->addComponent($Watermark);

$Namespace = $cf->php()->file()->Namespace()
    ->setData($df->php()->file()->Namespace()->namespace('Infotechnohelp\Cakephp'));

$File->addComponent($Namespace);

$Use = $cf->php()->file()->Use()
    ->setData($df->php()->file()->Use()->addUse('Infotechnohelp\Filesystem')->addUse('Cakephp\Table'));

$File->addComponent($Use);

$Class = $cf->php()->class()->Base()
    ->setData($df->php()->class()->Base()->title('ProductsTable')->extends('Table'))
    ->reserveIncluded($cf->php()->class()->Properties());

$File->addComponent($Class);

$Properties = $cf->php()->class()->Properties()
    ->setData(
        $df->php()->class()->Properties()
            ->setDefault($df->php()->class()->Properties()->getDefault()->visibility('public'))
            ->addProperty('id')
            ->addProperty('counter',
                $df->php()->class()->Properties()->Property()
                    ->visibility('private')
                    ->static()
                    ->value(0)
            )
            ->addProperty('title',
                $df->php()->class()->Properties()->Property()
                    ->value("'Product'")
            )
    );

$File->addComponent($Properties);

?>

<pre><?= (new BaseRecipe(null, null, (new RenderingSettings())->escapePhpTag()->return()))->renderBaseComponent($File) ?></pre>

<pre><?= (new BaseRecipe())->renderBaseComponent($File, (new RenderingSettings())->escapePhpTag()->return()) ?></pre>