<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/bootstrap.php';

use RenderScript\Lib\TwigRenderer\RenderingSettings;
use RenderScript\Extension\Recipe\Factory as RecipeFactory;
use RenderScript\Extension\ComponentData\Factory as ComponentDataFactory;
use RenderScript\Extension\PlainText\Factory as PlainTextFactory;
use RenderScript\Lib\Recipe\Data as RecipeData;

$df = new ComponentDataFactory();
$ptf = new PlainTextFactory();

$title = 'Extra';
$path = 'Self.ComponentItself';

$settings = (new RenderingSettings())->escapePhpTag()->return();

$selfComponentRecipeData = (new RecipeData())
    ->addComponentData(
        $df->php()->file()->Base()
            ->declareStrict()
    )
    ->addComponentData(
        $df->php()->file()->Namespace()
            ->namespace(
                'RenderScript\\Extension\Component\\' . implode("\\", explode(".", $path))
            )
    )
    ->addComponentData(
        $df->php()->file()->Use()
            ->addUse('RenderScript\Lib\Component')
    )
    ->addComponentData(
        $df->php()->class()->Base()
            ->title($title)
            ->extends('Component')
    )
    ->addComponentData(
        $df->self()->component()->Base()
            ->title($title)
    );

$selfComponentDataRecipeData = (new RecipeData())
    ->addComponentData($df->php()->file()->Base()
        ->declareStrict()
    )
    ->addComponentData($df->php()->file()->Namespace()
        ->namespace('RenderScript\\Extension\ComponentData\\' . implode("\\", explode(".", $path)))
    )
    ->addComponentData($df->php()->file()->Use()
        ->addUse('RenderScript\Lib\Component\Data as ComponentData')
    )
    ->addComponentData($df->php()->class()->Base()->title($title)
        ->extends('ComponentData')
    )
    ->addComponentData($df->plainText()->Base('Constructor')->text(
        $ptf->self()->ComponentData()->construct())
    )
    ->addComponentData($df->plainText()->Base('Setter method')->text(
        $ptf->self()->ComponentData()->setterMethod())
    );

$selfTemplateRecipeData = (new RecipeData())
    ->addComponentData(
        $df->plainText()->Base()->text('Twig template')
    );
?>

<h3>Component class</h3>

<pre><?= (new RecipeFactory())->self()
        ->Component($title, $path)->render($selfComponentRecipeData, $settings)
    ?></pre>

<h3>Component data class</h3>

<pre><?= (new RecipeFactory())->self()
        ->ComponentData($title, $path)->render($selfComponentDataRecipeData, $settings)
    ?></pre>

<h3>Factory classes</h3>

<pre>Automatic cascade rendering, no output available</pre>

<h3>Twig template file</h3>

<pre><?= (new RecipeFactory())->self()->Template($title, $path)->render($selfTemplateRecipeData, $settings) ?></pre>
