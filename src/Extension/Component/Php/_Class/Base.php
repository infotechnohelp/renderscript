<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\Php\_Class;

use RenderScript\Lib\Component;
use VendorTmp\TextModifier\StringModifier;

class Base extends Component
{
    public function __construct(string $componentTitle = null)
    {
        parent::__construct(__CLASS__, $componentTitle);
    }
}