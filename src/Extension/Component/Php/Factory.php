<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\Php;

use RenderScript\Extension\Component\Php\_Class\Factory as PhpClassFactory;
use RenderScript\Extension\Component\Php\File\Factory as PhpFileFactory;

class Factory
{
    public function file()
    {
        return new PhpFileFactory();
    }

    public function class()
    {
        return new PhpClassFactory();
    }
}