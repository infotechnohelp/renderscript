<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\Php\File;

use RenderScript\Lib\Component;

class Watermark extends Component
{
    public function __construct(string $componentTitle = null)
    {
        parent::__construct(__CLASS__, $componentTitle);
    }
}