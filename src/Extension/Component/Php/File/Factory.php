<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\Php\File;

class Factory
{
    public function Base(string $componentTitle = null)
    {
        return new Base($componentTitle);
    }

    public function Watermark(string $componentTitle = null)
    {
        return new Watermark($componentTitle);
    }

    public function Namespace(string $componentTitle = null)
    {
        return new _Namespace($componentTitle);
    }

    public function Use(string $componentTitle = null)
    {
        return new _Use($componentTitle);
    }
}