<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\Self\Factory;

class Factory
{
    public function Base(string $componentTitle = null)
    {
        return new Base($componentTitle);
    }
}