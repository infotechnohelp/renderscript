<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\Self;

use RenderScript\Extension\Component\Self\Component\Factory as ComponentFactory;
use RenderScript\Extension\Component\Self\Factory\Factory as FactoryFactory;

class Factory
{
    public function component()
    {
        return new ComponentFactory();
    }

    public function factory()
    {
        return new FactoryFactory();
    }
}