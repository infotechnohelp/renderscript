<?php declare(strict_types=1);

namespace RenderScript\Extension\Component\PlainText;

use RenderScript\Lib\Component;

class Base extends Component
{
    public function __construct(string $componentTitle = null)
    {
        parent::__construct(__CLASS__, $componentTitle);
    }
}