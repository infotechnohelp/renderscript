<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self\Template;

use Cake\Utility\Inflector;
use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\Recipe\ComponentStructure\Item as ComponentStructureItem;
use RenderScript\Lib\Recipe\RecipeInterface;
use VendorTmp\TextModifier\StringModifier;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

class Recipe extends BaseRecipe implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return (new ComponentStructure())
            ->addChild(
                new ComponentStructureItem($this->cf->plainText()->Base())
            );
    }

    public function render(RecipeData $RecipeData = null, RenderingSettings $Settings = null)
    {
        $this->fileTitle = StringModifier::ltrimDuplicates(Inflector::underscore($this->fileTitle), '_');

        $exploded = explode(".", $this->filePath);
        array_walk($exploded, function (&$value) {
            $value = StringModifier::ltrimDuplicates(Inflector::underscore($value), '_');
        });
        $templatePath = implode("/", $exploded) . "/";
        $customFilePath = ROOT_DIR . "src/templates/$templatePath";

        $Settings = $Settings ?? new RenderingSettings();

        $Settings->customFilePath($customFilePath)->fileExtension('twig');

        return parent::render($RecipeData, $Settings);
    }
}