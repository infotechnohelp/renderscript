<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self;

use RenderScript\Lib\Recipe\FactoryBase as RecipeFactory;
use RenderScript\Extension\Recipe\Self\Component\Recipe as SelfComponent;
use RenderScript\Extension\Recipe\Self\ComponentData\Recipe as SelfComponentData;
use RenderScript\Extension\Recipe\Self\Factory\Factory as SelfFactory;
use RenderScript\Extension\Recipe\Self\Template\Recipe as SelfTemplate;
use RenderScript\Extension\Recipe\Self\Recipe\Recipe as SelfRecipe;


class Factory extends RecipeFactory
{
    public function Component(string $fileTitle, string $filePath)
    {
        return new SelfComponent($fileTitle, $filePath);
    }

    public function ComponentData(string $fileTitle, string $filePath)
    {
        return new SelfComponentData($fileTitle, $filePath);
    }

    public function Template(string $fileTitle, string $filePath)
    {
        return new SelfTemplate($fileTitle, $filePath);
    }

    public function Recipe(string $fileTitle, string $filePath)
    {
        return new SelfRecipe($fileTitle, $filePath);
    }

    public function factory()
    {
        return new SelfFactory();
    }

}