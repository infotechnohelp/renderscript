<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self\ComponentData;

use RenderScript\Extension\Component\Factory as ComponentFactory;
use RenderScript\Extension\ComponentData\Factory as DataFactory;
use RenderScript\Extension\PlainText\Factory as PlainTextFactory;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\ComponentStructure\Item as ComponentStructureItem;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\Recipe\RecipeInterface;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

class Recipe extends BaseRecipe implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return (new ComponentStructure())
            ->addChild(
                (new ComponentStructureItem($this->cf->php()->file()->Base()))
                    ->addChild((new ComponentStructureItem($this->cf->php()->file()->Namespace())))
                    ->addChild((new ComponentStructureItem($this->cf->php()->file()->Use())))
                    ->addChild(
                        (new ComponentStructureItem($this->cf->php()->class()->Base()))
                            ->addChild((new ComponentStructureItem($this->cf->plainText()->Base('Constructor'))))
                            ->addChild((new ComponentStructureItem($this->cf->plainText()->Base('Setter method'))))
                    )
            );
    }

    public function render(RecipeData $RecipeData = null, RenderingSettings $Settings = null)
    {
        $customFilePath = SRC_DIR . "Extension/ComponentData/" . implode("/", explode(".", $this->filePath)) . "/";

        $Settings = $Settings ?? new RenderingSettings();
        $Settings->customFilePath($customFilePath);

        return parent::render($RecipeData, $Settings);
    }
}