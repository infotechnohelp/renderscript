<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self\Recipe;

use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\Recipe\ComponentStructure\Item as RecipeItem;
use RenderScript\Lib\Recipe\RecipeInterface;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

class Recipe extends BaseRecipe implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return (new ComponentStructure())
            ->addChild(
                (new RecipeItem($this->cf->php()->file()->Base()))
                    ->addChild(new RecipeItem($this->cf->php()->file()->Namespace()))
                    ->addChild(new RecipeItem($this->cf->php()->file()->Use()))
                    ->addChild(
                        (new RecipeItem($this->cf->php()->class()->Base()))
                            ->addChild(new RecipeItem($this->cf->plainText()->Base('Php Class')))
                    )
            );
    }

    public function render(RecipeData $RecipeData = null, RenderingSettings $Settings = null)
    {
        $customFilePath = SRC_DIR . 'Extension/Recipe/' . implode("/", explode(".", $this->filePath)) . "/{$this->fileTitle}/";

        $this->fileTitle = 'Recipe';

        $Settings = $Settings ?? new RenderingSettings();
        $Settings->customFilePath($customFilePath);

        return parent::render($RecipeData, $Settings);
    }
}