<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self\Factory;

use Infotechnohelp\Filesystem\Filesystem;
use RenderScript\Extension\Component\Factory as ComponentFactory;
use RenderScript\Extension\ComponentData\Factory as DataFactory;
use RenderScript\Lib\Component;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\TwigRenderer\RenderingSettings;
use Test\Utility\TestHelper;
use VendorTmp\TextModifier\PhpFileModifier;

class Recipe extends BaseRecipe //@todo ??? implements RecipeInterface
{
    private function renderFactory(string $title, string $filePath, string $namespace, RenderingSettings $settings = null)
    {
        $cf = new ComponentFactory();
        $df = new DataFactory();

        $File = $cf->Base()
            ->setFileInfo("$title.php", $filePath)
            ->reserveIncluded($cf->php()->file()->Base());

        $PhpFile = $cf->php()->file()->Base()
            ->setData($df->php()->file()->Base()->declareStrict())
            ->reserveIncluded($cf->php()->file()->Namespace())
            ->reserveIncluded($cf->php()->class()->Base());

        $File->addComponent($PhpFile);

        $Namespace = $cf->php()->file()->Namespace()
            ->setData($df->php()->file()->Namespace()->namespace($namespace));

        $File->addComponent($Namespace);

        $Class = $cf->php()->class()->Base()
            ->setData($df->php()->class()->Base()->title($title));

        $File->addComponent($Class);

        return parent::renderBaseComponent($File, $settings);
    }

    private function renderFactoryMethod(
        string $title,
        bool $final = false,
        string $alias = null,
        bool $isForRecipe = false
    )
    {
        $cf = new ComponentFactory();
        $df = new DataFactory();

        $BaseComponent = $cf->Base()
            ->setType(Component::STRING_TYPE)
            ->reserveIncluded($cf->self()->factory()->Base());

        $factoryMethod = $cf->self()->factory()->Base()
            ->setData(
                $df->self()->factory()->Base()
                    ->title($title)->final($final)->alias($alias)->isForRecipe($isForRecipe)
            );

        $BaseComponent->addComponent($factoryMethod);

        return (new BaseRecipe(null, null, (new RenderingSettings())->return()))->renderBaseComponent($BaseComponent);
    }

    protected function renderFactories(string $type, bool $isForRecipe = false, RenderingSettings $settings = null)
    {
        $typeBackslash = implode("\\", explode("/", $type));

        $uniquePathPart = '';
        $uniquePathPartBackslashes = '';
        $uniquePathPartParent = '';


        $srcPath = SRC_DIR;

        foreach (explode(".", $this->filePath) as $item) {

            $fs = new Filesystem("$srcPath/$type");

            $uniquePathPart .= "/$item";
            $uniquePathPartBackslashes .= "\\$item";

            if (!$fs->fileExists("$uniquePathPart/Factory.php")) {

                (new PhpFileModifier())->addUseToFile("$srcPath/$type$uniquePathPartParent/Factory.php",
                    "RenderScript\\$typeBackslash$uniquePathPartBackslashes\Factory as " .
                    ltrim("{$item}Factory", '_')
                );

                TestHelper::preparePath($srcPath);

                $factoryMethod = $this->renderFactoryMethod($item, false);

                $this->renderFactory(
                    'Factory',
                    "$srcPath/$type$uniquePathPart/",
                    "RenderScript\\$typeBackslash$uniquePathPartBackslashes",
                    $settings
                );

                (new PhpFileModifier())->addToClassEnd("$srcPath/$type$uniquePathPartParent/Factory.php",
                    "\n$factoryMethod"
                );
            }

            $uniquePathPartParent .= "/$item";
        }

        if (!$fs->fileExists("$uniquePathPartParent/Factory.php")) {
            $this->renderFactory(
                'Factory',
                "$srcPath/$type$uniquePathPartParent/",
                "RenderScript\\$typeBackslash$uniquePathPartBackslashes",
                $settings
            );
        }

        $alias = null;

        if ($isForRecipe) {

            $title = ltrim($this->fileTitle, '_');

            $pathArray = explode('.', $this->filePath);
            $alias = end($pathArray) . $title;

            (new PhpFileModifier())->addUseToFile("$srcPath/$type$uniquePathPartParent/Factory.php",
                "RenderScript\Extension\Recipe$uniquePathPartBackslashes\\{$this->fileTitle}\Recipe as $alias"
            );
        }

        $factoryMethod = $this->renderFactoryMethod($this->fileTitle, true, $alias, $isForRecipe);

        (new PhpFileModifier())->addToClassEnd("$srcPath/$type$uniquePathPartParent/Factory.php",
            "\n$factoryMethod"
        );
    }
}
