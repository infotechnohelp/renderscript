<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self\Factory;

use RenderScript\Extension\Recipe\Self\Factory\Component\Recipe as ComponentFactory;
use RenderScript\Extension\Recipe\Self\Factory\ComponentData\Recipe as DataFactory;
use RenderScript\Extension\Recipe\Self\Factory\Recipe\Recipe as RecipeFactory;
use RenderScript\Lib\Recipe\FactoryBase as RecipeFactoryBase;

class Factory extends RecipeFactoryBase
{
    public function Component(string $fileTitle, string $filePath)
    {
        return new ComponentFactory($fileTitle, $filePath);
    }

    public function ComponentData(string $fileTitle, string $filePath)
    {
        return new DataFactory($fileTitle, $filePath);
    }

    public function Recipe(string $fileTitle, string $filePath)
    {
        return new RecipeFactory($fileTitle, $filePath);
    }
}
