<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Self\Factory\ComponentData;

use RenderScript\Extension\Recipe\Self\Factory\Recipe as SelfFactoryBase;
use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\TwigRenderer\RenderingSettings;
use RenderScript\Lib\Recipe\RecipeInterface;

class Recipe extends SelfFactoryBase implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return null;
    }

    public function render(RecipeData $RecipeData = null, RenderingSettings $Settings = null)
    {
        $this->renderFactories('Extension/ComponentData', false, $Settings);
    }
}
