<?php declare(strict_types = 1);

namespace RenderScript\Extension\Recipe\Php;

use RenderScript\Extension\Recipe\Php\_Class\Recipe as PhpClass;

class Factory
{
    public function Class(string $fileTitle, string $filePath)
    {
        return new PhpClass($fileTitle, $filePath);
    }
}