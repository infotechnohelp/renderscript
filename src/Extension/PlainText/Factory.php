<?php declare(strict_types=1);

namespace RenderScript\Extension\PlainText;

use RenderScript\Extension\PlainText\Self\Factory as SelfFactory;

class Factory
{
    public function self()
    {
        return new SelfFactory();
    }
}