    public function title(string $value): self
    {
        $this->set('title', $value);

        return $this;
    }

