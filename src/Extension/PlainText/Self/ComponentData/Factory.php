<?php declare(strict_types=1);

namespace RenderScript\Extension\PlainText\Self\ComponentData;

use RenderScript\Lib\PlainText\FactoryBase;

class Factory extends FactoryBase
{
    public function setterMethod()
    {
        return $this->getContents(__FUNCTION__, __FILE__);
    }

    public function construct()
    {
        return $this->getContents(__FUNCTION__, __FILE__);
    }
}