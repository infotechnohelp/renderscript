<?php declare(strict_types=1);

namespace RenderScript\Extension\PlainText\Self;

use RenderScript\Extension\PlainText\Self\ComponentData\Factory as DataFactory;
use RenderScript\Extension\PlainText\Self\Recipe\Factory as RecipeFactory;

class Factory
{
    public function ComponentData()
    {
        return new DataFactory();
    }

    public function Recipe()
    {
        return new RecipeFactory();
    }
}