<?php declare(strict_types=1);

namespace RenderScript\Extension\PlainText\Self\Recipe;

use RenderScript\Lib\PlainText\FactoryBase;

class Factory extends FactoryBase
{
    public function recipeBase()
    {
        return $this->getContents(__FUNCTION__, __FILE__);
    }
}