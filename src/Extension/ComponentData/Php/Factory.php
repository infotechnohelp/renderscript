<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php;

use RenderScript\Extension\ComponentData\Php\File\Factory as PhpFileFactory;
use RenderScript\Extension\ComponentData\Php\_Class\Factory as PhpClassFactory;

class Factory
{
    public function file()
    {
        return new PhpFileFactory();
    }

    public function class()
    {
        return new PhpClassFactory();
    }
}