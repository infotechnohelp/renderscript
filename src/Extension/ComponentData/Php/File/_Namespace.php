<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php\File;

use RenderScript\Lib\Component\Data as ComponentData;

class _Namespace extends ComponentData
{
    public function namespace(string $value): self
    {
        $this->set('namespace', $value);

        return $this;
    }
}