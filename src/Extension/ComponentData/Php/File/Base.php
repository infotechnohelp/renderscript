<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php\File;

use RenderScript\Lib\Component\Data as ComponentData;

class Base extends ComponentData
{
    public function __construct(string $componentTitle = null)
    {
        $this->declareStrict(false);

        parent::__construct($componentTitle);
    }

    public function declareStrict(bool $value = true): self
    {
        $this->set('declareStrict', $value);

        return $this;
    }
}