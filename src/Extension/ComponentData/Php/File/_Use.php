<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php\File;

use RenderScript\Lib\Component\Data as ComponentData;

class _Use extends ComponentData
{
    public function addUse(string $value): self
    {
        $array = $this->get('use') ?? [];

        $array[] = $value;

        $this->set('use', array_unique($array));

        return $this;
    }
}