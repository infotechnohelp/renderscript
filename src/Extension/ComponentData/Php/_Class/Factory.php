<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php\_Class;

class Factory
{
    public function Base(string $componentTitle = null)
    {
        return new Base($componentTitle);
    }

    public function Properties(string $componentTitle = null)
    {
        return new Properties($componentTitle);
    }

    public function Getters(string $componentTitle = null)
    {
        return new Getters($componentTitle);
    }

    public function Setters(string $componentTitle = null)
    {
        return new Setters($componentTitle);
    }
}