<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Self;

use RenderScript\Extension\ComponentData\Self\Component\Factory as ComponentFactory;
use RenderScript\Extension\ComponentData\Self\Factory\Factory as FactoryFactory;

class Factory
{
    public function component()
    {
        return new ComponentFactory();
    }

    public function factory()
    {
        return new FactoryFactory();
    }

}