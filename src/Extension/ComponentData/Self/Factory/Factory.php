<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Self\Factory;

class Factory
{
    public function Base(string $componentTitle = null)
    {
        return new Base($componentTitle);
    }
}