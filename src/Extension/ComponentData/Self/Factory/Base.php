<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Self\Factory;

use RenderScript\Lib\Component\Data as ComponentData;

class Base extends ComponentData
{
    public function __construct(string $componentTitle = null)
    {
        $this
            ->final(false)
            ->isForRecipe(false);

        parent::__construct($componentTitle);
    }

    public function title(string $value): self
    {
        $this->set('title', $value);

        return $this;
    }

    public function alias(string $value = null): self
    {
        $this->set('alias', $value);

        return $this;
    }

    public function final(bool $value = true): self
    {
        $this->set('final', $value);

        return $this;
    }

    public function isForRecipe(bool $value = true): self
    {
        $this->set('isForRecipe', $value);

        return $this;
    }
}