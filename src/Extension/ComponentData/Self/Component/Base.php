<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Self\Component;

use RenderScript\Lib\Component\Data as ComponentData;

class Base extends ComponentData
{
    public function title(string $value): self
    {
        $this->set('title', $value);

        return $this;
    }
}