<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\PlainText;

use RenderScript\Lib\Component\Data as ComponentData;

class Base extends ComponentData
{
    public function text(string $value): self
    {
        $this->set('text', $value);

        return $this;
    }
}