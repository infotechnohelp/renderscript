<?php declare(strict_types=1);

namespace RenderScript\Lib\PlainText;

class FactoryBase
{
    protected function getContents(string $__FUNCTION__, string $__FILE__)
    {
        return file_get_contents(dirname($__FILE__) . "/$__FUNCTION__.txt");
    }
}