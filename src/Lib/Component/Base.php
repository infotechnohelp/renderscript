<?php declare(strict_types=1);

namespace RenderScript\Lib\Component;

use RenderScript\Lib\Component;

class Base extends Component
{
    public function __construct()
    {
        parent::__construct(__CLASS__, null, false, true);
    }
}