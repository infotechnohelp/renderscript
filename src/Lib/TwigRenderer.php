<?php declare(strict_types=1);

namespace RenderScript\Lib;

use Infotechnohelp\Filesystem\Filesystem;
use RenderScript\Lib\TwigRenderer\TwigFilters;
use RenderScript\Lib\TwigRenderer\RenderingSettings;
use Twig\Environment;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;

class TwigRenderer
{
    private Environment $twig;

    protected RenderingSettings $settings;

    private bool $isApi;

    public function __construct(string $rootDir = null, RenderingSettings $settings = null, bool $isApi = false)
    {
        $this->isApi = $isApi;

        $rootDir = $rootDir ?? ROOT_DIR;

        $loader = new TwigFilesystemLoader($rootDir);
        $this->twig = new Environment($loader, [
            'cache' => $rootDir . 'tmp/compilation_cache',
            'debug' => true,
        ]);

        (new TwigFilters())->addFiltersToTwig($this->twig);

        $this->settings = ($settings === null) ? new RenderingSettings() : $settings;
    }

    public function renderBaseComponent(Component $component, RenderingSettings $settings = null): ?string
    {
        $settings = ($settings === null) ? $this->settings : $settings;

        if (!$component->isBase()) {
            throw new \Exception('Component has parents'); // Create ComponentException later (separate file)
        }

        if ($component->getType() === Component::STRING_TYPE && $settings->getReturn() === false) {
            throw new \Exception('A component with a STRING_TYPE can return contents only');
        }

        $data = array_merge($component->toArray(), [
            'settings' => $settings->jsonSerialize()
        ]);

        $baseTemplatePath = ($this->isApi) ? 'vendor/renderscript/renderscript/src/Lib/templates/base.twig' : 'src/Lib/templates/base.twig';

        if ($settings->getReturn()) {
            return $this->twig->render($baseTemplatePath, $data);
        }

        $FileSystem = new Filesystem();
        $filePath = $component->getFilePath() . $component->getFileTitle();

        if (!$settings->getRewrite() && $FileSystem->fileExists($filePath)) {
            return null;
        }

        $FileSystem->write(
            $filePath,
            $this->twig->render($baseTemplatePath, $data)
        );

        return null;
    }
}