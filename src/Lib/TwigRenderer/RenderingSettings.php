<?php declare(strict_types=1);

namespace RenderScript\Lib\TwigRenderer;

use RenderScript\Lib\Component\Data as ComponentData;

class RenderingSettings extends ComponentData
{
    public function __construct()
    {
        $this
            ->escapePhpTag(false)
            ->return(false)
            ->rewrite()
            ->fileExtension('php')
            ->customFilePath(null);
    }

    public function escapePhpTag(bool $value = true): self
    {
        $this->set('escapePhpTag', $value);

        return $this;
    }

    public function return(bool $value = true): self
    {
        $this->set('return', $value);

        return $this;
    }

    public function rewrite(bool $value = true): self
    {
        $this->set('rewrite', $value);

        return $this;
    }

    public function getReturn(): bool
    {
        return $this->get('return');
    }

    public function getRewrite(): bool
    {
        return $this->get('rewrite');
    }

    public function customFilePath(string $value = null): self
    {
        $this->set('customFilePath', $value);

        return $this;
    }

    public function getCustomFilePath(): ?string
    {
        return $this->get('customFilePath');
    }

    public function fileExtension(string $value = null): self
    {
        $this->set('fileExtension', $value);

        return $this;
    }

    public function getFileExtension(): string
    {
        return $this->get('fileExtension');
    }
}