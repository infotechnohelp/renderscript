<?php declare(strict_types=1);

namespace RenderScript\Lib;

use RenderScript\Extension\Component\Factory as ComponentFactory;
use RenderScript\Extension\ComponentData\Factory as DataFactory;
use RenderScript\Lib\Component\Data as ComponentData;
use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

class Recipe extends TwigRenderer
{
    protected ComponentFactory $cf;

    protected DataFactory $df;

    protected ?string $fileTitle;

    protected ?string $filePath;

    private string $srcDir;

    public function __construct(string $fileTitle = null, string $filePath = null, RenderingSettings $settings = null, string $twigRendererRootDir = null, bool $isApi = false)
    {
        parent::__construct($twigRendererRootDir, $settings, $isApi);

        $this->cf = new ComponentFactory();
        $this->df = new DataFactory();
        $this->fileTitle = $fileTitle;
        $this->filePath = $filePath;

        $this->srcDir = $twigRendererRootDir . 'src/';
    }

    private function getComponentPathArray(Component $Component)
    {
        if ($Component->getComponentTitle() !== $Component->getDefaultComponentTitle()) {
            return [$Component->getComponentTitle()];
        }
        return explode('\\', explode('Extension\\Component\\', get_class($Component))[1]);
    }

    public function dataIsSetFor(Component $component, RecipeData $data): bool
    {
        if ($this->recursiveIsset($data->getData(), $this->getComponentPathArray($component))) {
            return true;
        }

        return false;
    }

    protected function getCustomDataFor(Component $component, RecipeData $data): ComponentData
    {
        $dataArray = $data->getData();
        $dataLocation = &$dataArray;

        foreach ($this->getComponentPathArray($component) as $key) {
            $dataLocation = &$dataLocation[$key];
        }

        return $dataLocation;
    }

    // @todo LATER Should be separated (Infotechnohelp/PhpHelper)
    # By stackoverflow.com user: user1327498
    # (https://stackoverflow.com/questions/4576671/php-testing-for-existence-of-a-cell-in-a-multidimensional-array)
    private function recursiveIsset($variable, $checkArray, $i = 0)
    {
        $new_var = null;
        if (is_array($variable) && array_key_exists($checkArray[$i], $variable))
            $new_var = $variable[$checkArray[$i]];
        else if (is_object($variable) && array_key_exists($checkArray[$i], $variable))
            $new_var = $variable->$checkArray[$i];
        if (!isset($new_var))
            return false;

        else if (count($checkArray) > $i + 1)
            return $this->recursiveIsset($new_var, $checkArray, $i + 1);
        else
            return $new_var;
    }

    private function processComponent($componentData, RecipeData $data, Component\Base $File)
    {
        /** @var Component $Component */
        $Component = $componentData['component'];

        if ($this->dataIsSetFor($Component, $data)) {
            $customData = $this->getCustomDataFor($Component, $data);

            if (isset($componentData['dataModifier'])) {
                $componentData['dataModifier']($customData);
            }

            $Component->setData($customData);

        } else {

            if (isset($componentData['dataModifier'])) {
                $defaultData = $Component->initDefaultData();

                $componentData['dataModifier']($defaultData);

                $Component->setData($defaultData);
            }

        }


        if (isset($componentData['children'])) {
            foreach ($componentData['children'] as $childComponentData) {
                $Component->reserveIncluded($childComponentData['component']);

                $this->processComponent($childComponentData, $data, $File);
            }
        }

        $File->addComponent($Component);
    }

    protected function populateBaseComponent(?ComponentStructure $componentStructure, RecipeData $RecipeData, Component\Base $File)
    {
        $componentStructure = $componentStructure ?? new ComponentStructure();

        $componentStructure->validate();

        foreach ($componentStructure->getData()['children'] as $componentData) {
            $File->reserveIncluded($componentData['component']);
            $this->processComponent($componentData, $RecipeData, $File);
        }
    }

    public function render(RecipeData $RecipeData = null, RenderingSettings $Settings = null)
    {
        $filePath = $Settings->getCustomFilePath() ?? $this->srcDir . implode("/", explode(".", $this->filePath)) . "/";

        $BaseComponent = $this->cf->Base()
            ->setFileInfo("{$this->fileTitle}.{$Settings->getFileExtension()}", $filePath);

        $this->populateBaseComponent(
            $this->getRecipeComponentStructure(),
            $RecipeData ?? new RecipeData(),
            $BaseComponent
        );

        return $this->renderBaseComponent($BaseComponent, $Settings);
    }
}