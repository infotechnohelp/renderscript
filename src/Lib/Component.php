<?php declare(strict_types=1);

namespace RenderScript\Lib;

use Cake\Utility\Inflector;
use Test\Utility\TestHelper;
use RenderScript\Lib\Component\Data as ComponentData;
use VendorTmp\TextModifier\StringModifier;

class Component implements \JsonSerializable
{
    const FILE_TYPE = 'file';
    const STRING_TYPE = 'string';

    private string $template;

    private string $componentTitle;

    private string $defaultComponentTitle;

    private string $type;

    private ?string $fileTitle = null;

    private ?string $filePath = null;

    private array $reservedIncludes = [];

    private ?Component $parent = null;

    private ?ComponentData $data = null;

    private array $components = [];

    public function __construct(string $__CLASS__, string $customTitle = null, bool $isApi = false, bool $isBase = false)
    {
        $prefix = ($isBase) ? "RenderScript\Lib\Component\\": "RenderScript\Extension\Component\\";

        $prefix = ($isApi) ? "RenderScript\Api\Extension\Component\\" : $prefix;

        if (strpos($__CLASS__, $prefix) !== 0){
            throw new \Exception("$__CLASS__ is not a correct extension component");
        }

        $class = substr($__CLASS__, strlen($prefix));

        $exploded = explode('\\', $class);

        $templateTitleData = $exploded;

        array_walk($templateTitleData, function (&$value) {
            $value = Inflector::underscore(StringModifier::lcfirst($value, '_'));
        });

        $template = implode("/", $templateTitleData) . ".twig";

        $templatePathPrefix = ($isBase) ? 'src/Lib/templates/': 'src/Extension/templates/';

        $componentTitle = implode('.', $exploded);

        $this->defaultComponentTitle = $componentTitle;
        $this->componentTitle = $customTitle ?? $componentTitle;
        $this->template = $templatePathPrefix . $template;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, [self::FILE_TYPE, self::STRING_TYPE], true)) {
            $this->throwException("Type '$type' is invalid");
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return null
     */
    public function getFileTitle()
    {
        return $this->fileTitle;
    }

    /**
     * @return null
     */
    public function getFilePath()
    {
        if (!getenv('RENDERSCRIPT_TEST')) {
            return $this->filePath;
        }

        return TestHelper::preparePath($this->filePath, true);
    }


    public function isBase()
    {
        return $this->parent === null;
    }

    public function getComponentTitle(): string
    {
        return $this->componentTitle;
    }

    public function getDefaultComponentTitle(): string
    {
        return $this->defaultComponentTitle;
    }

    public function setFileInfo(string $title, string $path): self
    {
        $this->fileTitle = $title;
        $this->filePath = $path;

        return $this;
    }


    public function reserveIncluded(Component $component): self
    {
        $this->reservedIncludes[] = $component->getComponentTitle();

        return $this;
    }

    public function setParent(Component $component): self
    {
        $this->parent = $component;

        return $this;
    }

    public function addComponent(Component $component): self
    {
        $this->components[$component->getComponentTitle()] =
            $component
                ->setParent($this)
                ->setType(Component::STRING_TYPE);

        return $this;
    }

    private function throwException(string $msg)
    {
        throw new \Exception($msg . ': ' . json_encode($this->jsonSerialize(false)));
    }

    private function componentIncluded(string $title)
    {
        $components = ($this->parent === null) ? $this->components : $this->parent->components;

        return array_key_exists($title, $components);
    }

    private function validateIncluded()
    {
        foreach ($this->reservedIncludes as $reservedInclude) {
            if (!$this->componentIncluded($reservedInclude)) {
                $this->throwException("Component '$reservedInclude' missing");
            }
        }
    }

    private function validate()
    {
        if (!isset($this->type)) {
            $this->throwException('Type is not set');
        }

        if ($this->parent === null) {
            if ($this->type === self::FILE_TYPE) {
                if (empty($this->fileTitle) || empty($this->filePath)) {
                    $this->throwException('File info is incomplete');
                }
            }
        } else {
            if ($this->template === 'src/Lib/templates/base.twig') {
                $this->throwException('Template is not set');
            }
        }


        $this->validateIncluded();
    }

    public function setData(ComponentData $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function jsonSerialize(bool $validate = true)
    {
        if ($validate) {
            $this->validate();
        }

        return [
            'config' => [
                'title' => $this->fileTitle,
                'path' => $this->filePath,
                'template' => $this->template,
                'include' => $this->reservedIncludes,
            ],
            'data' => $this->data,
            'components' => $this->components,
        ];
    }

    public function initDefaultData()
    {
        $dataClassName =
            str_replace('RenderScript\\Extension\\Component', 'RenderScript\\Extension\\ComponentData', get_class($this));

        return new $dataClassName();
    }

    public function debug(bool $return = false): ?string
    {
        $result = print_r($this->jsonSerialize(), true);

        if ($return) {
            return $result;
        }
        echo '<pre>' . $result . '</pre>';

        return null;
    }

    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }
}