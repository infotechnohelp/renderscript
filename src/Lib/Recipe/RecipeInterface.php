<?php declare(strict_types=1);

namespace RenderScript\Lib\Recipe;

use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

interface RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure;

    public function render(RecipeData $RecipeData = null, RenderingSettings $settings = null);
}