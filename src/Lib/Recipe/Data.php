<?php declare(strict_types=1);

namespace RenderScript\Lib\Recipe;

use RenderScript\Lib\Component\Data as ComponentData;

// @todo ??? Extends DataContainer
class Data
{
    public $data = [];

    private function getDataPathArray(ComponentData $Data)
    {
        if($Data->getComponentTitle() !== null){
            return [$Data->getComponentTitle()];
        }


        return explode('\\', explode('Extension\\ComponentData\\', get_class($Data))[1]);
    }

    public function addComponentData(ComponentData $Data): self
    {
        $location = &$this->data;

        foreach ($this->getDataPathArray($Data) as $key) {
            $location = &$location[$key];
        }

        $componentTitle = implode(".", $this->getDataPathArray($Data));

        if(isset($location)){
            throw new \Exception("Duplicated recipe data item '$componentTitle'");
        }

        $location = $Data;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}