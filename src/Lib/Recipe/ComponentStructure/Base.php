<?php declare(strict_types=1);

namespace RenderScript\Lib\Recipe\ComponentStructure;

use RenderScript\Lib\Recipe\ComponentStructure\Item as ComponentStructureItem;

// @todo ??? extends DataContainer
class Base
{
    protected $data = [

    ];

    public function addChild(ComponentStructureItem $componentRecipeItem): self
    {
        $this->data['children'][] = $componentRecipeItem->getData();

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}