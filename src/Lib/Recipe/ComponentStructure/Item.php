<?php declare(strict_types=1);

namespace RenderScript\Lib\Recipe\ComponentStructure;

use RenderScript\Lib\Component;
use RenderScript\Lib\Recipe\ComponentStructure\Base as ComponentStructureBase;

// @todo ??? extends DataContainer (ComponentStructure)
class Item extends ComponentStructureBase
{
    public function __construct(Component $Component)
    {
        $this->data['component'] = $Component;
    }

    public function addDataModifier(callable $callable): self
    {
        $this->data['dataModifier'] = $callable;

        return $this;
    }
}