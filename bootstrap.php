<?php declare(strict_types=1);

define('ROOT_DIR', __DIR__ . DIRECTORY_SEPARATOR);
define('SRC_DIR', ROOT_DIR . 'src' . DIRECTORY_SEPARATOR);